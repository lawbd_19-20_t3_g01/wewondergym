﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WeWonderGym.Models
{
    public partial class ExercicoPlano
    {
        [Display(Name = "Nome do Exercicio")]
        public int IdExercicio { get; set; }
        public int IdPlano { get; set; }
        [Display(Name = "Duração do Periodo de descanso")]
        public string DuracaoPeriodoDescanso { get; set; }
        [Display(Name = "Quantidade de Séries")]
        public int QuantidadeSeries { get; set; }
        [Display(Name = "Número de Repetições")]
        public int NumeroRepeticoes { get; set; }
        [Display(Name = "Duração dos Exercícios")]
        public string DuracaoExercicios { get; set; }
        public int Ordem { get; set; }

        public virtual ExercicioGinasio IdExercicioNavigation { get; set; }
        public virtual PlanoExercicio IdPlanoNavigation { get; set; }
    }
}
