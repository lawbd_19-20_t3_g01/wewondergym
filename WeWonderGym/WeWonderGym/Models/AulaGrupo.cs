﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WeWonderGym.Models
{
    public partial class AulaGrupo
    {
        public AulaGrupo()
        {
            Aula = new HashSet<Aula>();
        }

        public int IdAulaGrupo { get; set; }
        public int IdAdmin { get; set; }
        public string Foto { get; set; }
        public string Nome { get; set; }
        [Display(Name = "Descrição")]
        public string Descricao { get; set; }

        public virtual Administrador IdAdminNavigation { get; set; }
        public virtual ICollection<Aula> Aula { get; set; }
    }
}
