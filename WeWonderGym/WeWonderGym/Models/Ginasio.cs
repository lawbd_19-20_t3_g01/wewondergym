﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WeWonderGym.Models
{
    public partial class Ginasio
    {
        public int IdGinasio { get; set; }
        [Display(Name ="Localização GPS")]
        public string DadosLocalizacaoGps { get; set; }
        [Display(Name = "Endereço")]
        public string Endereco { get; set; }
        public string Email { get; set; }
        [Display(Name = "Número de telefone")]
        public string NumTelefone { get; set; }
        [Display(Name = "Número de telemovel")]
        public string NumTelemovel { get; set; }
        public string Semana { get; set; }
    }
}
