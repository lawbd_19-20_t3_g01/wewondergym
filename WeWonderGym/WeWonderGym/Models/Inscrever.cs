﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WeWonderGym.Models
{
    public partial class Inscrever
    {
        public int IdAula { get; set; }
        public int IdSocio { get; set; }
        [Display(Name = "Data de inscrição")]
        public DateTime DataAula { get; set; }

        public virtual Aula IdAulaNavigation { get; set; }
        public virtual Socio IdSocioNavigation { get; set; }
    }
}
