﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WeWonderGym.Models
{
    public partial class Peso
    {
        public int IdProfessor { get; set; }
        public int IdSocio { get; set; }
        public DateTime DataCriacao { get; set; }
        [Display(Name="Peso")]
        public double Valor { get; set; }

        public virtual Professor IdProfessorNavigation { get; set; }
        public virtual Socio IdSocioNavigation { get; set; }
    }
}
