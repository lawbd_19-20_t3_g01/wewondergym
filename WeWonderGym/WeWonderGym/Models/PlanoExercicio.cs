﻿using System;
using System.Collections.Generic;

namespace WeWonderGym.Models
{
    public partial class PlanoExercicio
    {
        public PlanoExercicio()
        {
            ExercicoPlano = new HashSet<ExercicoPlano>();
        }

        public int IdPlano { get; set; }
        public int IdProfessor { get; set; }
        public int IdSocio { get; set; }
        public string Descricao { get; set; }
        public bool? Ativo { get; set; }

        public virtual Professor IdProfessorNavigation { get; set; }
        public virtual Socio IdSocioNavigation { get; set; }
        public virtual ICollection<ExercicoPlano> ExercicoPlano { get; set; }
    }
}
