﻿using System;
using System.Collections.Generic;

namespace WeWonderGym.Models
{
    public partial class ExercicioGinasio
    {
        public ExercicioGinasio()
        {
            ExercicoPlano = new HashSet<ExercicoPlano>();
        }

        public int IdExercicio { get; set; }
        public int IdAdmin { get; set; }
        public string Foto { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Video { get; set; }

        public virtual Administrador IdAdminNavigation { get; set; }
        public virtual ICollection<ExercicoPlano> ExercicoPlano { get; set; }
    }
}
