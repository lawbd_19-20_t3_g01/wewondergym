﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WeWonderGym.Models
{
    public partial class Aula
    {
        public Aula()
        {
            Inscrever = new HashSet<Inscrever>();
        }

        public int IdAula { get; set; }
        [Display(Name = "Professor")]
        public int IdProfessor { get; set; }
        [Display(Name = "Aula Grupo")]
        public int IdAulaGrupo { get; set; }
        public int IdAdmin { get; set; }
        public int Lotacao { get; set; }
        [Display(Name = "Validade até:")]
        public DateTime ValidadeAte { get; set; }
        [Display(Name = "Validade de:")]
        public DateTime ValidadeDe { get; set; }
        [Display(Name = "Dia da Semana")]
        public string DiaDaSemana { get; set; }
        public string Semana { get; set; }
        public string Hora { get; set; }

        public virtual Administrador IdAdminNavigation { get; set; }
        public virtual AulaGrupo IdAulaGrupoNavigation { get; set; }
        public virtual Professor IdProfessorNavigation { get; set; }
        public virtual ICollection<Inscrever> Inscrever { get; set; }
    }
}
