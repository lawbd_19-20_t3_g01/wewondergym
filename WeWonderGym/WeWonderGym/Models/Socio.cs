﻿using System;
using System.Collections.Generic;

namespace WeWonderGym.Models
{
    public partial class Socio
    {
        public Socio()
        {
            Inscrever = new HashSet<Inscrever>();
            PedidoPt = new HashSet<PedidoPt>();
            Peso = new HashSet<Peso>();
            PlanoExercicio = new HashSet<PlanoExercicio>();
        }

        public int IdUtilizador { get; set; }
        public int IdSocio { get; set; }
        public string Altura { get; set; }

        public virtual Utilizador IdUtilizadorNavigation { get; set; }
        public virtual ICollection<Inscrever> Inscrever { get; set; }
        public virtual ICollection<PedidoPt> PedidoPt { get; set; }
        public virtual ICollection<Peso> Peso { get; set; }
        public virtual ICollection<PlanoExercicio> PlanoExercicio { get; set; }
    }
}
