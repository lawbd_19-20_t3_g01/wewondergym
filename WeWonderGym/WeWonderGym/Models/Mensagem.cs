﻿using System;
using System.Collections.Generic;

namespace WeWonderGym.Models
{
    public partial class Mensagem
    {
        public int IdMensagem { get; set; }
        public int UtilizadorDe { get; set; }
        public int UtilizadorPara { get; set; }
        public string Texto { get; set; }
        public bool Arquivada { get; set; }
        public bool Lida { get; set; }
        public DateTime DataEnvio { get; set; }
        public DateTime DataRecebido { get; set; }

        public virtual Utilizador UtilizadorDeNavigation { get; set; }
        public virtual Utilizador UtilizadorParaNavigation { get; set; }
    }
}
