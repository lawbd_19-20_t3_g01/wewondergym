﻿using System;
using System.Collections.Generic;

namespace WeWonderGym.Models
{
    public partial class Administrador
    {
        public Administrador()
        {
            Aula = new HashSet<Aula>();
            AulaGrupo = new HashSet<AulaGrupo>();
            ExercicioGinasio = new HashSet<ExercicioGinasio>();
            PedidoPt = new HashSet<PedidoPt>();
            Utilizador = new HashSet<Utilizador>();
        }

        public int IdAdmin { get; set; }
        public string UserName { get; set; }
        public string PalavraPass { get; set; }

        public virtual ICollection<Aula> Aula { get; set; }
        public virtual ICollection<AulaGrupo> AulaGrupo { get; set; }
        public virtual ICollection<ExercicioGinasio> ExercicioGinasio { get; set; }
        public virtual ICollection<PedidoPt> PedidoPt { get; set; }
        public virtual ICollection<Utilizador> Utilizador { get; set; }
    }
}
