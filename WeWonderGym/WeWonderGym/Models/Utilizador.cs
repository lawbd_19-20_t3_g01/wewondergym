﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WeWonderGym.Models
{
    public partial class Utilizador
    {
        public Utilizador()
        {
            MensagemUtilizadorDeNavigation = new HashSet<Mensagem>();
            MensagemUtilizadorParaNavigation = new HashSet<Mensagem>();
            Socio = new HashSet<Socio>();
        }

        public int IdUtilizador { get; set; }
        public int IdAdmin { get; set; }
        [Display(Name = "Número de Telefone")]
        [MinLength(9)]
        [MaxLength(12)]
        [Required]
        public string NumTelefone { get; set; }
        public string Nome { get; set; }
        [Display(Name = "Fotografia")]
        public string Fotografica { get; set; }
        public int Estado { get; set; }
        public string Sexo { get; set; }
        [DataType(DataType.EmailAddress, ErrorMessage = "Este campo tem de ser um email valido!")]
        public string Email { get; set; }
        [Display(Name = "Password")]
        public string PalavraPass { get; set; }
        public string EstadoMotivo { get; set; }

        public virtual Administrador IdAdminNavigation { get; set; }
        public virtual Professor Professor { get; set; }
        public virtual ICollection<Mensagem> MensagemUtilizadorDeNavigation { get; set; }
        public virtual ICollection<Mensagem> MensagemUtilizadorParaNavigation { get; set; }
        public virtual ICollection<Socio> Socio { get; set; }
    }
}
