﻿using System;
using System.Collections.Generic;

namespace WeWonderGym.Models
{
    public partial class PedidoPt
    {
        public int IdPedido { get; set; }
        public DateTime DataPedido { get; set; }
        public int Estado { get; set; }
        public int IdAdmin { get; set; }
        public int IdProfessor { get; set; }
        public int IdSocio { get; set; }

        public virtual Administrador IdAdminNavigation { get; set; }
        public virtual Professor IdProfessorNavigation { get; set; }
        public virtual Socio IdSocioNavigation { get; set; }
    }
}
