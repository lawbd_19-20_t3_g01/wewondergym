﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeWonderGym.Models
{
    public class GinasioAulas
    {
        public Ginasio GinasioModel { get; set; }
        public IEnumerable<WeWonderGym.Models.Aula> AulaModel { get; set; }
    }
}

