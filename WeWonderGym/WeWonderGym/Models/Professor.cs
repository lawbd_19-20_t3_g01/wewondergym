﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WeWonderGym.Models
{
    public partial class Professor
    {
        public Professor()
        {
            Aula = new HashSet<Aula>();
            PedidoPt = new HashSet<PedidoPt>();
            Peso = new HashSet<Peso>();
            PlanoExercicio = new HashSet<PlanoExercicio>();
        }

        public int IdUtilizador { get; set; }
        [Display(Name = "Professor")]
        public int IdProfessor { get; set; }
        public string Especialidade { get; set; }

        public virtual Utilizador IdUtilizadorNavigation { get; set; }
        public virtual ICollection<Aula> Aula { get; set; }
        public virtual ICollection<PedidoPt> PedidoPt { get; set; }
        public virtual ICollection<Peso> Peso { get; set; }
        public virtual ICollection<PlanoExercicio> PlanoExercicio { get; set; }
    }
}
