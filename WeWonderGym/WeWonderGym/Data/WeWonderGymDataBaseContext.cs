﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using WeWonderGym.Models;

namespace WeWonderGym.Data
{
    public partial class WeWonderGymDataBaseContext : DbContext
    {
        public WeWonderGymDataBaseContext()
        {
        }

        public WeWonderGymDataBaseContext(DbContextOptions<WeWonderGymDataBaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Administrador> Administrador { get; set; }
        public virtual DbSet<Aula> Aula { get; set; }
        public virtual DbSet<AulaGrupo> AulaGrupo { get; set; }
        public virtual DbSet<ExercicioGinasio> ExercicioGinasio { get; set; }
        public virtual DbSet<ExercicoPlano> ExercicoPlano { get; set; }
        public virtual DbSet<Ginasio> Ginasio { get; set; }
        public virtual DbSet<Inscrever> Inscrever { get; set; }
        public virtual DbSet<Mensagem> Mensagem { get; set; }
        public virtual DbSet<PedidoPt> PedidoPt { get; set; }
        public virtual DbSet<Peso> Peso { get; set; }
        public virtual DbSet<PlanoExercicio> PlanoExercicio { get; set; }
        public virtual DbSet<Professor> Professor { get; set; }
        public virtual DbSet<Socio> Socio { get; set; }
        public virtual DbSet<Utilizador> Utilizador { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=WeWonderGymDataBase;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Administrador>(entity =>
            {
                entity.HasKey(e => e.IdAdmin)
                    .HasName("PK__Administ__B2C3ADE55C311ABD");

                entity.Property(e => e.IdAdmin).HasColumnName("idAdmin");

                entity.Property(e => e.PalavraPass)
                    .IsRequired()
                    .HasColumnName("palavraPass")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnName("userName")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Aula>(entity =>
            {
                entity.HasKey(e => e.IdAula)
                    .HasName("PK__Aula__D861CCCB10700950");

                entity.Property(e => e.IdAula).HasColumnName("idAula");

                entity.Property(e => e.DiaDaSemana)
                    .IsRequired()
                    .HasColumnName("diaDaSemana")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Hora)
                    .IsRequired()
                    .HasColumnName("hora")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.IdAdmin).HasColumnName("idAdmin");

                entity.Property(e => e.IdAulaGrupo).HasColumnName("idAulaGrupo");

                entity.Property(e => e.IdProfessor).HasColumnName("idProfessor");

                entity.Property(e => e.Lotacao)
                    .HasColumnName("lotacao")
                    .HasDefaultValueSql("((25))");

                entity.Property(e => e.Semana)
                    .IsRequired()
                    .HasColumnName("semana")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.ValidadeAte)
                    .HasColumnName("validadeAte")
                    .HasColumnType("datetime");

                entity.Property(e => e.ValidadeDe)
                    .HasColumnName("validadeDe")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.IdAdminNavigation)
                    .WithMany(p => p.Aula)
                    .HasForeignKey(d => d.IdAdmin)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Aula__idAdmin__45F365D3");

                entity.HasOne(d => d.IdAulaGrupoNavigation)
                    .WithMany(p => p.Aula)
                    .HasForeignKey(d => d.IdAulaGrupo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Aula__idAulaGrup__44FF419A");

                entity.HasOne(d => d.IdProfessorNavigation)
                    .WithMany(p => p.Aula)
                    .HasForeignKey(d => d.IdProfessor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Aula__idProfesso__440B1D61");
            });

            modelBuilder.Entity<AulaGrupo>(entity =>
            {
                entity.HasKey(e => e.IdAulaGrupo)
                    .HasName("PK__AulaGrup__07520A85F3467757");

                entity.Property(e => e.IdAulaGrupo).HasColumnName("idAulaGrupo");

                entity.Property(e => e.Descricao)
                    .IsRequired()
                    .HasColumnName("descricao")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Foto)
                    .IsRequired()
                    .HasColumnName("foto")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdAdmin).HasColumnName("idAdmin");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAdminNavigation)
                    .WithMany(p => p.AulaGrupo)
                    .HasForeignKey(d => d.IdAdmin)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__AulaGrupo__idAdm__403A8C7D");
            });

            modelBuilder.Entity<ExercicioGinasio>(entity =>
            {
                entity.HasKey(e => e.IdExercicio)
                    .HasName("PK__Exercici__5330B3C4975FEA77");

                entity.Property(e => e.IdExercicio).HasColumnName("idExercicio");

                entity.Property(e => e.Descricao)
                    .IsRequired()
                    .HasColumnName("descricao")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Foto)
                    .IsRequired()
                    .HasColumnName("foto")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.IdAdmin).HasColumnName("idAdmin");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Video)
                    .IsRequired()
                    .HasColumnName("video")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAdminNavigation)
                    .WithMany(p => p.ExercicioGinasio)
                    .HasForeignKey(d => d.IdAdmin)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Exercicio__idAdm__5165187F");
            });

            modelBuilder.Entity<ExercicoPlano>(entity =>
            {
                entity.HasKey(e => new { e.IdExercicio, e.IdPlano, e.Ordem })
                    .HasName("PK__Exercico__5D8A6D2BBEA1DB56");

                entity.ToTable("Exercico_Plano");

                entity.Property(e => e.IdExercicio).HasColumnName("idExercicio");

                entity.Property(e => e.IdPlano).HasColumnName("idPlano");

                entity.Property(e => e.Ordem).HasColumnName("ordem");

                entity.Property(e => e.DuracaoExercicios)
                    .IsRequired()
                    .HasColumnName("duracaoExercicios")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.DuracaoPeriodoDescanso)
                    .IsRequired()
                    .HasColumnName("duracaoPeriodoDescanso")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.NumeroRepeticoes).HasColumnName("numeroRepeticoes");

                entity.Property(e => e.QuantidadeSeries).HasColumnName("quantidadeSeries");

                entity.HasOne(d => d.IdExercicioNavigation)
                    .WithMany(p => p.ExercicoPlano)
                    .HasForeignKey(d => d.IdExercicio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Exercico___idExe__5441852A");

                entity.HasOne(d => d.IdPlanoNavigation)
                    .WithMany(p => p.ExercicoPlano)
                    .HasForeignKey(d => d.IdPlano)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Exercico___idPla__5535A963");
            });

            modelBuilder.Entity<Ginasio>(entity =>
            {
                entity.HasKey(e => e.IdGinasio)
                    .HasName("PK__Ginasio__D069463D9A1CE6F0");

                entity.Property(e => e.IdGinasio)
                    .HasColumnName("idGinasio")
                    .ValueGeneratedNever();

                entity.Property(e => e.DadosLocalizacaoGps)
                    .IsRequired()
                    .HasColumnName("dadosLocalizacaoGPS")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Endereco)
                    .IsRequired()
                    .HasColumnName("endereco")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumTelefone)
                    .IsRequired()
                    .HasColumnName("numTelefone")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.NumTelemovel)
                    .IsRequired()
                    .HasColumnName("numTelemovel")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Semana)
                    .IsRequired()
                    .HasColumnName("semana")
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Inscrever>(entity =>
            {
                entity.HasKey(e => new { e.IdAula, e.IdSocio })
                    .HasName("PK__Inscreve__D678BA57AF6A6ADF");

                entity.Property(e => e.IdAula).HasColumnName("idAula");

                entity.Property(e => e.IdSocio).HasColumnName("idSocio");

                entity.Property(e => e.DataAula)
                    .HasColumnName("dataAula")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.IdAulaNavigation)
                    .WithMany(p => p.Inscrever)
                    .HasForeignKey(d => d.IdAula)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Inscrever__idAul__48CFD27E");

                entity.HasOne(d => d.IdSocioNavigation)
                    .WithMany(p => p.Inscrever)
                    .HasForeignKey(d => d.IdSocio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Inscrever__idSoc__49C3F6B7");
            });

            modelBuilder.Entity<Mensagem>(entity =>
            {
                entity.HasKey(e => e.IdMensagem)
                    .HasName("PK__Mensagem__AAFB640DD7AFD5DF");

                entity.Property(e => e.IdMensagem).HasColumnName("idMensagem");

                entity.Property(e => e.Arquivada).HasColumnName("arquivada");

                entity.Property(e => e.DataEnvio)
                    .HasColumnName("dataEnvio")
                    .HasColumnType("datetime");

                entity.Property(e => e.DataRecebido)
                    .HasColumnName("dataRecebido")
                    .HasColumnType("datetime");

                entity.Property(e => e.Lida).HasColumnName("lida");

                entity.Property(e => e.Texto)
                    .IsRequired()
                    .HasColumnName("texto")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.UtilizadorDe).HasColumnName("utilizadorDe");

                entity.Property(e => e.UtilizadorPara).HasColumnName("utilizadorPara");

                entity.HasOne(d => d.UtilizadorDeNavigation)
                    .WithMany(p => p.MensagemUtilizadorDeNavigation)
                    .HasForeignKey(d => d.UtilizadorDe)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Mensagem__utiliz__36B12243");

                entity.HasOne(d => d.UtilizadorParaNavigation)
                    .WithMany(p => p.MensagemUtilizadorParaNavigation)
                    .HasForeignKey(d => d.UtilizadorPara)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Mensagem__utiliz__37A5467C");
            });

            modelBuilder.Entity<PedidoPt>(entity =>
            {
                entity.HasKey(e => e.IdPedido)
                    .HasName("PK__PedidoPT__A9F619B736D51B6E");

                entity.ToTable("PedidoPT");

                entity.Property(e => e.IdPedido).HasColumnName("idPedido");

                entity.Property(e => e.DataPedido)
                    .HasColumnName("dataPedido")
                    .HasColumnType("datetime");

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.IdAdmin).HasColumnName("idAdmin");

                entity.Property(e => e.IdProfessor).HasColumnName("idProfessor");

                entity.Property(e => e.IdSocio).HasColumnName("idSocio");

                entity.HasOne(d => d.IdAdminNavigation)
                    .WithMany(p => p.PedidoPt)
                    .HasForeignKey(d => d.IdAdmin)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PedidoPT__idAdmi__70DDC3D8");

                entity.HasOne(d => d.IdProfessorNavigation)
                    .WithMany(p => p.PedidoPt)
                    .HasForeignKey(d => d.IdProfessor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PedidoPT__idProf__71D1E811");

                entity.HasOne(d => d.IdSocioNavigation)
                    .WithMany(p => p.PedidoPt)
                    .HasForeignKey(d => d.IdSocio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PedidoPT__idSoci__72C60C4A");
            });

            modelBuilder.Entity<Peso>(entity =>
            {
                entity.HasKey(e => new { e.IdProfessor, e.IdSocio, e.DataCriacao })
                    .HasName("PK__Peso__D8C0FE5934CE049D");

                entity.Property(e => e.IdProfessor).HasColumnName("idProfessor");

                entity.Property(e => e.IdSocio).HasColumnName("idSocio");

                entity.Property(e => e.DataCriacao)
                    .HasColumnName("dataCriacao")
                    .HasColumnType("datetime");

                entity.Property(e => e.Valor).HasColumnName("valor");

                entity.HasOne(d => d.IdProfessorNavigation)
                    .WithMany(p => p.Peso)
                    .HasForeignKey(d => d.IdProfessor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Peso__idProfesso__30F848ED");

                entity.HasOne(d => d.IdSocioNavigation)
                    .WithMany(p => p.Peso)
                    .HasForeignKey(d => d.IdSocio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Peso__idSocio__31EC6D26");
            });

            modelBuilder.Entity<PlanoExercicio>(entity =>
            {
                entity.HasKey(e => e.IdPlano)
                    .HasName("PK__PlanoExe__39B8602283E81141");

                entity.Property(e => e.IdPlano).HasColumnName("idPlano");

                entity.Property(e => e.Ativo)
                    .IsRequired()
                    .HasColumnName("ativo")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Descricao)
                    .IsRequired()
                    .HasColumnName("descricao")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IdProfessor).HasColumnName("idProfessor");

                entity.Property(e => e.IdSocio).HasColumnName("idSocio");

                entity.HasOne(d => d.IdProfessorNavigation)
                    .WithMany(p => p.PlanoExercicio)
                    .HasForeignKey(d => d.IdProfessor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PlanoExer__idPro__4D94879B");

                entity.HasOne(d => d.IdSocioNavigation)
                    .WithMany(p => p.PlanoExercicio)
                    .HasForeignKey(d => d.IdSocio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PlanoExer__idSoc__4E88ABD4");
            });

            modelBuilder.Entity<Professor>(entity =>
            {
                entity.HasKey(e => e.IdProfessor)
                    .HasName("PK__Professo__4E7C3C6D744F4F4D");

                entity.HasIndex(e => e.IdUtilizador)
                    .HasName("UQ__Professo__9F6A66A264074F02")
                    .IsUnique();

                entity.Property(e => e.IdProfessor)
                    .HasColumnName("idProfessor")
                    .ValueGeneratedNever();

                entity.Property(e => e.Especialidade)
                    .IsRequired()
                    .HasColumnName("especialidade")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IdUtilizador).HasColumnName("idUtilizador");

                entity.HasOne(d => d.IdUtilizadorNavigation)
                    .WithOne(p => p.Professor)
                    .HasForeignKey<Professor>(d => d.IdUtilizador)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Professor__idUti__2A4B4B5E");
            });

            modelBuilder.Entity<Socio>(entity =>
            {
                entity.HasKey(e => e.IdSocio)
                    .HasName("PK__Socio__E19769C105574170");

                entity.HasIndex(e => e.IdSocio)
                    .HasName("UQ__Socio__E19769C06C444B4D")
                    .IsUnique();

                entity.Property(e => e.IdSocio)
                    .HasColumnName("idSocio")
                    .ValueGeneratedNever();

                entity.Property(e => e.Altura)
                    .IsRequired()
                    .HasColumnName("altura")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IdUtilizador).HasColumnName("idUtilizador");

                entity.HasOne(d => d.IdUtilizadorNavigation)
                    .WithMany(p => p.Socio)
                    .HasForeignKey(d => d.IdUtilizador)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Socio__idUtiliza__2E1BDC42");
            });

            modelBuilder.Entity<Utilizador>(entity =>
            {
                entity.HasKey(e => e.IdUtilizador)
                    .HasName("PK__Utilizad__9F6A66A39FEC2D48");

                entity.HasIndex(e => e.Email)
                    .HasName("UQ__Utilizad__AB6E6164724A3C6E")
                    .IsUnique();

                entity.Property(e => e.IdUtilizador).HasColumnName("idUtilizador");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.EstadoMotivo)
                    .IsRequired()
                    .HasColumnName("estadoMotivo")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Fotografica)
                    .IsRequired()
                    .HasColumnName("fotografica")
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.IdAdmin).HasColumnName("idAdmin");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumTelefone)
                    .IsRequired()
                    .HasColumnName("numTelefone")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PalavraPass)
                    .IsRequired()
                    .HasColumnName("palavraPass")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Sexo)
                    .IsRequired()
                    .HasColumnName("sexo")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAdminNavigation)
                    .WithMany(p => p.Utilizador)
                    .HasForeignKey(d => d.IdAdmin)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Utilizado__idAdm__267ABA7A");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
