﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using WeWonderGym.Models;
using MySql.Data.EntityFrameworkCore;
using System.IO;

namespace WeWonderGym.Data
{
    public class InicializarDB
    {
        public static void Iniciar(WeWonderGymDataBaseContext contexto)
        {
            // verifica e garante que a BD existe
            contexto.Database.EnsureCreated();

            // analiza a(s) tabela(s) onde pretendemos garantir os dados
            if (contexto.Ginasio.Any() == false)
            {
                var gym = new Ginasio();

                gym.IdGinasio = 0;
                gym.NumTelefone = "259 123 515";
                gym.NumTelemovel = "914 589 532";
                gym.Email = "contacts@wewonder.pt";
                gym.Endereco = "R. Padre Filipe Borges 9, 5000-439 Vila Real";
                gym.DadosLocalizacaoGps = "41.30636111111111, -7.746138888888889";
                gym.Semana = "1";
     
                //... insere-os no model...
                contexto.Ginasio.Add(gym);
               
                //...e atualiza a base de dados
                contexto.SaveChanges();
            }

            if(contexto.Administrador.Any() == false)
            {
             
                SHA512 sha512 = SHA512Managed.Create();
                byte[] bytes = sha512.ComputeHash(Encoding.UTF8.GetBytes("wewonderadmin123"), 0, Encoding.UTF8.GetByteCount("wewonderadmin123"));


                string passHash = Convert.ToBase64String(bytes);

                var admin = new Administrador();

                admin.IdAdmin = 0;
                admin.UserName = "WonderAdmin";
                admin.PalavraPass = passHash;
                contexto.Administrador.Add(admin);
                contexto.SaveChanges();
            }
            if (contexto.Professor.Any() == false)
            {
                SHA512 sha512 = SHA512Managed.Create();
                byte[] bytes = sha512.ComputeHash(Encoding.UTF8.GetBytes("123456"), 0, Encoding.UTF8.GetByteCount("123456"));
                string passHash = Convert.ToBase64String(bytes);

                Utilizador uu = new Utilizador();
                Utilizador uu1 = new Utilizador();
                Utilizador uu2 = new Utilizador();
                Utilizador uu3 = new Utilizador();

                uu.Nome = "Ethel Davis";
                uu.Email = "etheldavies@wewonder.pt";
                uu.NumTelefone = "120941283";
                uu.Fotografica = "t1.jpg";
                uu.Estado = 1;
                uu.IdAdmin = 1;
                uu.IdAdminNavigation = contexto.Administrador.FirstOrDefault(x => x.IdAdmin == 0);
                uu.Sexo = "Feminino";
                uu.PalavraPass = passHash;
                uu.EstadoMotivo = "Ativo";
                uu.Professor = new Professor { Especialidade = "Zumba", IdProfessor = 1 + 2 };
                contexto.Add(uu);

                uu1.Nome = "Rodney Cooper";
                uu1.Email = "RodneyCooper@wewonder.pt";
                uu1.Fotografica = "t2.jpg";
                uu1.NumTelefone = "421932546";
                uu1.Estado = 1;
                uu1.IdAdmin = 1;
                uu1.Sexo = "Masculino";
                uu1.PalavraPass = passHash;
                uu1.EstadoMotivo = "Ativo";
                uu1.Professor = new Professor { Especialidade = "Ferro", IdProfessor = 2 + 2 };
                contexto.Add(uu1);

                uu2.Nome = "Dora Walker";
                uu2.Email = "DoraWalker@wewonder.pt";
                uu2.NumTelefone = "141254621";
                uu2.Fotografica = "t3.jpg";
                uu2.Estado = 1;
                uu2.IdAdmin = 1;
                uu2.Sexo = "Feminino";
                uu2.PalavraPass = passHash;
                uu2.EstadoMotivo = "Ativo";
                uu2.Professor = new Professor { Especialidade = "Pilates", IdProfessor = 3 + 2 };
                contexto.Add(uu2);


                uu3.Nome = "Lena Keller";
                uu3.Email = "LenaKeller@wewonder.pt";
                uu3.Fotografica = "t4.jpg";
                uu3.NumTelefone = "141231221";
                uu3.Estado = 1;
                uu3.IdAdmin = 1;
                uu3.Sexo = "Masculino";
                uu3.PalavraPass = passHash;
                uu3.EstadoMotivo = "Ativo";
                uu3.Professor = new Professor { Especialidade = "CrossFit", IdProfessor = 4 + 2 };
                contexto.Add(uu3);

               

                }

            if (contexto.AulaGrupo.Any() == false)
            {
                AulaGrupo a1 = new AulaGrupo();
                AulaGrupo a2 = new AulaGrupo();
                AulaGrupo a3 = new AulaGrupo();
                AulaGrupo a4 = new AulaGrupo();
                AulaGrupo a5 = new AulaGrupo();
                AulaGrupo a6 = new AulaGrupo();

                a1.IdAdmin = 1;
                a1.Foto = "c1.jpg";
                a1.Nome = "Running Classes";
                a1.Descricao = "Correr faz bem!";
                contexto.Add(a1);

                a2.IdAdmin = 1;
                a2.Foto = "c2.jpg";
                a2.Nome = "Weight Lifting Classes";
                a2.Descricao = "Para um bom estar físico";
                contexto.Add(a2);

                a3.IdAdmin = 1;
                a3.Foto = "c3.jpg";
                a3.Nome = "Body Combat Classes";
                a3.Descricao = "É um bom estar em forma!";
                contexto.Add(a3);

                a4.IdAdmin = 1;
                a4.Foto = "c4.jpg";
                a4.Nome = "Organic Yoga Classes";
                a4.Descricao = "Para um bom estar físico e mental";
                contexto.Add(a4);


                a5.IdAdmin = 1;
                a5.Foto = "c5.jpg";
                a5.Nome = "Raw Fitness Classes";
                a5.Descricao = "Para um bom ganho da massa muscular";
                contexto.Add(a5);

                a6.IdAdmin = 1;
                a6.Foto = "c6.jpg";
                a6.Nome = "Body Building Classes";
                a6.Descricao = "Há que ficar grande!";
                contexto.Add(a6);


                contexto.SaveChanges();
            }
        }
    }
}
