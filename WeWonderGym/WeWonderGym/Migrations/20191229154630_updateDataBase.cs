﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WeWonderGym.Migrations
{
    public partial class updateDataBase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__AulaGrupo__idAdm__08B54D69",
                table: "AulaGrupo");

            migrationBuilder.DropForeignKey(
                name: "FK__Exercicio__idAdm__5629CD9C",
                table: "ExercicioGinasio");

            migrationBuilder.DropForeignKey(
                name: "FK__Exercico___idExe__17036CC0",
                table: "Exercico_Plano");

            migrationBuilder.DropForeignKey(
                name: "FK__Exercico___idPla__17F790F9",
                table: "Exercico_Plano");

            migrationBuilder.DropForeignKey(
                name: "FK__Mensagem__utiliz__04E4BC85",
                table: "Mensagem");

            migrationBuilder.DropForeignKey(
                name: "FK__Mensagem__utiliz__05D8E0BE",
                table: "Mensagem");

            migrationBuilder.DropForeignKey(
                name: "FK__Peso__idProfesso__7E37BEF6",
                table: "Peso");

            migrationBuilder.DropForeignKey(
                name: "FK__Peso__idSocio__7F2BE32F",
                table: "Peso");

            migrationBuilder.DropForeignKey(
                name: "FK__PlanoExer__idPro__0F624AF8",
                table: "PlanoExercicio");

            migrationBuilder.DropForeignKey(
                name: "FK__PlanoExer__idSoc__10566F31",
                table: "PlanoExercicio");

            migrationBuilder.DropForeignKey(
                name: "FK__Professor__idUti__778AC167",
                table: "Professor");

            migrationBuilder.DropForeignKey(
                name: "FK__Socio__idUtiliza__7B5B524B",
                table: "Socio");

            migrationBuilder.DropForeignKey(
                name: "FK__Utilizado__idAdm__73BA3083",
                table: "Utilizador");

            migrationBuilder.DropPrimaryKey(
                name: "PK__Utilizad__9F6A66A3D059DAC1",
                table: "Utilizador");

            migrationBuilder.DropPrimaryKey(
                name: "PK__Socio__E19769C1AFD53D0E",
                table: "Socio");

            migrationBuilder.DropPrimaryKey(
                name: "PK__Professo__4E7C3C6DB004F223",
                table: "Professor");

            migrationBuilder.DropPrimaryKey(
                name: "PK__PlanoExe__39B860229320CE31",
                table: "PlanoExercicio");

            migrationBuilder.DropPrimaryKey(
                name: "PK__Peso__D8C0FE596FC9DD7A",
                table: "Peso");

            migrationBuilder.DropPrimaryKey(
                name: "PK__Mensagem__AAFB640DCE9F3493",
                table: "Mensagem");

            migrationBuilder.DropPrimaryKey(
                name: "PK__Exercico__5D8A6D2B6503DCF7",
                table: "Exercico_Plano");

            migrationBuilder.DropPrimaryKey(
                name: "PK__Exercici__5330B3C4556A8F6A",
                table: "ExercicioGinasio");

            migrationBuilder.DropPrimaryKey(
                name: "PK__AulaGrup__07520A8552010D58",
                table: "AulaGrupo");

            migrationBuilder.DropPrimaryKey(
                name: "PK__Administ__B2C3ADE5AF5DDEC1",
                table: "Administrador");

            migrationBuilder.RenameIndex(
                name: "UQ__Utilizad__AB6E6164FF1F4950",
                table: "Utilizador",
                newName: "UQ__Utilizad__AB6E6164A662D9B0");

            migrationBuilder.RenameIndex(
                name: "UQ__Socio__E19769C080D37256",
                table: "Socio",
                newName: "UQ__Socio__E19769C00C4FDDE6");

            migrationBuilder.RenameIndex(
                name: "UQ__Professo__9F6A66A2C11C0D2E",
                table: "Professor",
                newName: "UQ__Professo__9F6A66A268ED4576");

            migrationBuilder.AddColumn<int>(
                name: "idGinasio",
                table: "Ginasio",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK__Utilizad__9F6A66A3EEC5DACC",
                table: "Utilizador",
                column: "idUtilizador");

            migrationBuilder.AddPrimaryKey(
                name: "PK__Socio__E19769C13F13DD27",
                table: "Socio",
                column: "idSocio");

            migrationBuilder.AddPrimaryKey(
                name: "PK__Professo__4E7C3C6D32F1007B",
                table: "Professor",
                column: "idProfessor");

            migrationBuilder.AddPrimaryKey(
                name: "PK__PlanoExe__39B86022E10C8032",
                table: "PlanoExercicio",
                column: "idPlano");

            migrationBuilder.AddPrimaryKey(
                name: "PK__Peso__D8C0FE5933FA4707",
                table: "Peso",
                columns: new[] { "idProfessor", "idSocio", "dataCriacao" });

            migrationBuilder.AddPrimaryKey(
                name: "PK__Mensagem__AAFB640DA3F787A5",
                table: "Mensagem",
                column: "idMensagem");

            migrationBuilder.AddPrimaryKey(
                name: "PK__Ginasio__D069463D8424F381",
                table: "Ginasio",
                column: "idGinasio");

            migrationBuilder.AddPrimaryKey(
                name: "PK__Exercico__5D8A6D2B529FED46",
                table: "Exercico_Plano",
                columns: new[] { "idExercicio", "idPlano", "ordem" });

            migrationBuilder.AddPrimaryKey(
                name: "PK__Exercici__5330B3C4C56DDE5C",
                table: "ExercicioGinasio",
                column: "idExercicio");

            migrationBuilder.AddPrimaryKey(
                name: "PK__AulaGrup__07520A85C54C2634",
                table: "AulaGrupo",
                column: "idAulaGrupo");

            migrationBuilder.AddPrimaryKey(
                name: "PK__Administ__B2C3ADE5E47117B1",
                table: "Administrador",
                column: "idAdmin");

            migrationBuilder.CreateTable(
                name: "Aula",
                columns: table => new
                {
                    idAula = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    idProfessor = table.Column<int>(nullable: false),
                    idAulaGrupo = table.Column<int>(nullable: false),
                    idAdmin = table.Column<int>(nullable: false),
                    lotacao = table.Column<int>(nullable: false, defaultValueSql: "((25))"),
                    validadeAte = table.Column<DateTime>(type: "datetime", nullable: false),
                    validadeDe = table.Column<DateTime>(type: "datetime", nullable: false),
                    diaDaSemana = table.Column<string>(unicode: false, maxLength: 25, nullable: false),
                    semana = table.Column<string>(unicode: false, maxLength: 25, nullable: false),
                    hora = table.Column<string>(unicode: false, maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Aula__D861CCCB3DB027C7", x => x.idAula);
                    table.ForeignKey(
                        name: "FK__Aula__idAdmin__3F466844",
                        column: x => x.idAdmin,
                        principalTable: "Administrador",
                        principalColumn: "idAdmin",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__Aula__idAulaGrup__3E52440B",
                        column: x => x.idAulaGrupo,
                        principalTable: "AulaGrupo",
                        principalColumn: "idAulaGrupo",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__Aula__idProfesso__3D5E1FD2",
                        column: x => x.idProfessor,
                        principalTable: "Professor",
                        principalColumn: "idProfessor",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PedidoPT",
                columns: table => new
                {
                    dataPedido = table.Column<DateTime>(type: "datetime", nullable: false),
                    estado = table.Column<int>(nullable: false),
                    idAdmin = table.Column<int>(nullable: false),
                    idProfessor = table.Column<int>(nullable: false),
                    idSocio = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__PedidoPT__80DA3F0DEAE09B1B", x => x.dataPedido);
                    table.ForeignKey(
                        name: "FK__PedidoPT__idAdmi__6FE99F9F",
                        column: x => x.idAdmin,
                        principalTable: "Administrador",
                        principalColumn: "idAdmin",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__PedidoPT__idProf__70DDC3D8",
                        column: x => x.idProfessor,
                        principalTable: "Professor",
                        principalColumn: "idProfessor",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__PedidoPT__idSoci__71D1E811",
                        column: x => x.idSocio,
                        principalTable: "Socio",
                        principalColumn: "idSocio",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Inscrever",
                columns: table => new
                {
                    idAula = table.Column<int>(nullable: false),
                    idSocio = table.Column<int>(nullable: false),
                    dataAula = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Inscreve__D678BA57D652D83E", x => new { x.idAula, x.idSocio });
                    table.ForeignKey(
                        name: "FK__Inscrever__idAul__4222D4EF",
                        column: x => x.idAula,
                        principalTable: "Aula",
                        principalColumn: "idAula",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__Inscrever__idSoc__4316F928",
                        column: x => x.idSocio,
                        principalTable: "Socio",
                        principalColumn: "idSocio",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Aula_idAdmin",
                table: "Aula",
                column: "idAdmin");

            migrationBuilder.CreateIndex(
                name: "IX_Aula_idAulaGrupo",
                table: "Aula",
                column: "idAulaGrupo");

            migrationBuilder.CreateIndex(
                name: "IX_Aula_idProfessor",
                table: "Aula",
                column: "idProfessor");

            migrationBuilder.CreateIndex(
                name: "IX_Inscrever_idSocio",
                table: "Inscrever",
                column: "idSocio");

            migrationBuilder.CreateIndex(
                name: "IX_PedidoPT_idAdmin",
                table: "PedidoPT",
                column: "idAdmin");

            migrationBuilder.CreateIndex(
                name: "IX_PedidoPT_idProfessor",
                table: "PedidoPT",
                column: "idProfessor");

            migrationBuilder.CreateIndex(
                name: "IX_PedidoPT_idSocio",
                table: "PedidoPT",
                column: "idSocio");

            migrationBuilder.AddForeignKey(
                name: "FK__AulaGrupo__idAdm__398D8EEE",
                table: "AulaGrupo",
                column: "idAdmin",
                principalTable: "Administrador",
                principalColumn: "idAdmin",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Exercicio__idAdm__4AB81AF0",
                table: "ExercicioGinasio",
                column: "idAdmin",
                principalTable: "Administrador",
                principalColumn: "idAdmin",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Exercico___idExe__4D94879B",
                table: "Exercico_Plano",
                column: "idExercicio",
                principalTable: "ExercicioGinasio",
                principalColumn: "idExercicio",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Exercico___idPla__4E88ABD4",
                table: "Exercico_Plano",
                column: "idPlano",
                principalTable: "PlanoExercicio",
                principalColumn: "idPlano",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Mensagem__utiliz__5EBF139D",
                table: "Mensagem",
                column: "utilizadorDe",
                principalTable: "Utilizador",
                principalColumn: "idUtilizador",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Mensagem__utiliz__5FB337D6",
                table: "Mensagem",
                column: "utilizadorPara",
                principalTable: "Utilizador",
                principalColumn: "idUtilizador",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Peso__idProfesso__30F848ED",
                table: "Peso",
                column: "idProfessor",
                principalTable: "Professor",
                principalColumn: "idProfessor",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Peso__idSocio__31EC6D26",
                table: "Peso",
                column: "idSocio",
                principalTable: "Socio",
                principalColumn: "idSocio",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__PlanoExer__idPro__46E78A0C",
                table: "PlanoExercicio",
                column: "idProfessor",
                principalTable: "Professor",
                principalColumn: "idProfessor",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__PlanoExer__idSoc__47DBAE45",
                table: "PlanoExercicio",
                column: "idSocio",
                principalTable: "Socio",
                principalColumn: "idSocio",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Professor__idUti__2A4B4B5E",
                table: "Professor",
                column: "idUtilizador",
                principalTable: "Utilizador",
                principalColumn: "idUtilizador",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Socio__idUtiliza__2E1BDC42",
                table: "Socio",
                column: "idUtilizador",
                principalTable: "Utilizador",
                principalColumn: "idUtilizador",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Utilizado__idAdm__267ABA7A",
                table: "Utilizador",
                column: "idAdmin",
                principalTable: "Administrador",
                principalColumn: "idAdmin",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__AulaGrupo__idAdm__398D8EEE",
                table: "AulaGrupo");

            migrationBuilder.DropForeignKey(
                name: "FK__Exercicio__idAdm__4AB81AF0",
                table: "ExercicioGinasio");

            migrationBuilder.DropForeignKey(
                name: "FK__Exercico___idExe__4D94879B",
                table: "Exercico_Plano");

            migrationBuilder.DropForeignKey(
                name: "FK__Exercico___idPla__4E88ABD4",
                table: "Exercico_Plano");

            migrationBuilder.DropForeignKey(
                name: "FK__Mensagem__utiliz__5EBF139D",
                table: "Mensagem");

            migrationBuilder.DropForeignKey(
                name: "FK__Mensagem__utiliz__5FB337D6",
                table: "Mensagem");

            migrationBuilder.DropForeignKey(
                name: "FK__Peso__idProfesso__30F848ED",
                table: "Peso");

            migrationBuilder.DropForeignKey(
                name: "FK__Peso__idSocio__31EC6D26",
                table: "Peso");

            migrationBuilder.DropForeignKey(
                name: "FK__PlanoExer__idPro__46E78A0C",
                table: "PlanoExercicio");

            migrationBuilder.DropForeignKey(
                name: "FK__PlanoExer__idSoc__47DBAE45",
                table: "PlanoExercicio");

            migrationBuilder.DropForeignKey(
                name: "FK__Professor__idUti__2A4B4B5E",
                table: "Professor");

            migrationBuilder.DropForeignKey(
                name: "FK__Socio__idUtiliza__2E1BDC42",
                table: "Socio");

            migrationBuilder.DropForeignKey(
                name: "FK__Utilizado__idAdm__267ABA7A",
                table: "Utilizador");

            migrationBuilder.DropTable(
                name: "Inscrever");

            migrationBuilder.DropTable(
                name: "PedidoPT");

            migrationBuilder.DropTable(
                name: "Aula");

            migrationBuilder.DropPrimaryKey(
                name: "PK__Utilizad__9F6A66A3EEC5DACC",
                table: "Utilizador");

            migrationBuilder.DropPrimaryKey(
                name: "PK__Socio__E19769C13F13DD27",
                table: "Socio");

            migrationBuilder.DropPrimaryKey(
                name: "PK__Professo__4E7C3C6D32F1007B",
                table: "Professor");

            migrationBuilder.DropPrimaryKey(
                name: "PK__PlanoExe__39B86022E10C8032",
                table: "PlanoExercicio");

            migrationBuilder.DropPrimaryKey(
                name: "PK__Peso__D8C0FE5933FA4707",
                table: "Peso");

            migrationBuilder.DropPrimaryKey(
                name: "PK__Mensagem__AAFB640DA3F787A5",
                table: "Mensagem");

            migrationBuilder.DropPrimaryKey(
                name: "PK__Ginasio__D069463D8424F381",
                table: "Ginasio");

            migrationBuilder.DropPrimaryKey(
                name: "PK__Exercico__5D8A6D2B529FED46",
                table: "Exercico_Plano");

            migrationBuilder.DropPrimaryKey(
                name: "PK__Exercici__5330B3C4C56DDE5C",
                table: "ExercicioGinasio");

            migrationBuilder.DropPrimaryKey(
                name: "PK__AulaGrup__07520A85C54C2634",
                table: "AulaGrupo");

            migrationBuilder.DropPrimaryKey(
                name: "PK__Administ__B2C3ADE5E47117B1",
                table: "Administrador");

            migrationBuilder.DropColumn(
                name: "idGinasio",
                table: "Ginasio");

            migrationBuilder.RenameIndex(
                name: "UQ__Utilizad__AB6E6164A662D9B0",
                table: "Utilizador",
                newName: "UQ__Utilizad__AB6E6164FF1F4950");

            migrationBuilder.RenameIndex(
                name: "UQ__Socio__E19769C00C4FDDE6",
                table: "Socio",
                newName: "UQ__Socio__E19769C080D37256");

            migrationBuilder.RenameIndex(
                name: "UQ__Professo__9F6A66A268ED4576",
                table: "Professor",
                newName: "UQ__Professo__9F6A66A2C11C0D2E");

            migrationBuilder.AddPrimaryKey(
                name: "PK__Utilizad__9F6A66A3D059DAC1",
                table: "Utilizador",
                column: "idUtilizador");

            migrationBuilder.AddPrimaryKey(
                name: "PK__Socio__E19769C1AFD53D0E",
                table: "Socio",
                column: "idSocio");

            migrationBuilder.AddPrimaryKey(
                name: "PK__Professo__4E7C3C6DB004F223",
                table: "Professor",
                column: "idProfessor");

            migrationBuilder.AddPrimaryKey(
                name: "PK__PlanoExe__39B860229320CE31",
                table: "PlanoExercicio",
                column: "idPlano");

            migrationBuilder.AddPrimaryKey(
                name: "PK__Peso__D8C0FE596FC9DD7A",
                table: "Peso",
                columns: new[] { "idProfessor", "idSocio", "dataCriacao" });

            migrationBuilder.AddPrimaryKey(
                name: "PK__Mensagem__AAFB640DCE9F3493",
                table: "Mensagem",
                column: "idMensagem");

            migrationBuilder.AddPrimaryKey(
                name: "PK__Exercico__5D8A6D2B6503DCF7",
                table: "Exercico_Plano",
                columns: new[] { "idExercicio", "idPlano", "ordem" });

            migrationBuilder.AddPrimaryKey(
                name: "PK__Exercici__5330B3C4556A8F6A",
                table: "ExercicioGinasio",
                column: "idExercicio");

            migrationBuilder.AddPrimaryKey(
                name: "PK__AulaGrup__07520A8552010D58",
                table: "AulaGrupo",
                column: "idAulaGrupo");

            migrationBuilder.AddPrimaryKey(
                name: "PK__Administ__B2C3ADE5AF5DDEC1",
                table: "Administrador",
                column: "idAdmin");

            migrationBuilder.AddForeignKey(
                name: "FK__AulaGrupo__idAdm__08B54D69",
                table: "AulaGrupo",
                column: "idAdmin",
                principalTable: "Administrador",
                principalColumn: "idAdmin",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Exercicio__idAdm__5629CD9C",
                table: "ExercicioGinasio",
                column: "idAdmin",
                principalTable: "Administrador",
                principalColumn: "idAdmin",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Exercico___idExe__17036CC0",
                table: "Exercico_Plano",
                column: "idExercicio",
                principalTable: "ExercicioGinasio",
                principalColumn: "idExercicio",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Exercico___idPla__17F790F9",
                table: "Exercico_Plano",
                column: "idPlano",
                principalTable: "PlanoExercicio",
                principalColumn: "idPlano",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Mensagem__utiliz__04E4BC85",
                table: "Mensagem",
                column: "utilizadorDe",
                principalTable: "Utilizador",
                principalColumn: "idUtilizador",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Mensagem__utiliz__05D8E0BE",
                table: "Mensagem",
                column: "utilizadorPara",
                principalTable: "Utilizador",
                principalColumn: "idUtilizador",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Peso__idProfesso__7E37BEF6",
                table: "Peso",
                column: "idProfessor",
                principalTable: "Professor",
                principalColumn: "idProfessor",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Peso__idSocio__7F2BE32F",
                table: "Peso",
                column: "idSocio",
                principalTable: "Socio",
                principalColumn: "idSocio",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__PlanoExer__idPro__0F624AF8",
                table: "PlanoExercicio",
                column: "idProfessor",
                principalTable: "Professor",
                principalColumn: "idProfessor",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__PlanoExer__idSoc__10566F31",
                table: "PlanoExercicio",
                column: "idSocio",
                principalTable: "Socio",
                principalColumn: "idSocio",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Professor__idUti__778AC167",
                table: "Professor",
                column: "idUtilizador",
                principalTable: "Utilizador",
                principalColumn: "idUtilizador",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Socio__idUtiliza__7B5B524B",
                table: "Socio",
                column: "idUtilizador",
                principalTable: "Utilizador",
                principalColumn: "idUtilizador",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Utilizado__idAdm__73BA3083",
                table: "Utilizador",
                column: "idAdmin",
                principalTable: "Administrador",
                principalColumn: "idAdmin",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
