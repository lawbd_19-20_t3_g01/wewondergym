﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WeWonderGym.Migrations
{
    public partial class criarDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Administrador",
                columns: table => new
                {
                    idAdmin = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    userName = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    palavraPass = table.Column<string>(unicode: false, maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Administ__B2C3ADE5AF5DDEC1", x => x.idAdmin);
                });

            migrationBuilder.CreateTable(
                name: "Ginasio",
                columns: table => new
                {
                    dadosLocalizacaoGPS = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    endereco = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    email = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    numTelefone = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    numTelemovel = table.Column<string>(unicode: false, maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "AulaGrupo",
                columns: table => new
                {
                    idAulaGrupo = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    idAdmin = table.Column<int>(nullable: false),
                    foto = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    nome = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    descricao = table.Column<string>(unicode: false, maxLength: 150, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__AulaGrup__07520A8552010D58", x => x.idAulaGrupo);
                    table.ForeignKey(
                        name: "FK__AulaGrupo__idAdm__08B54D69",
                        column: x => x.idAdmin,
                        principalTable: "Administrador",
                        principalColumn: "idAdmin",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ExercicioGinasio",
                columns: table => new
                {
                    idExercicio = table.Column<int>(nullable: false),
                    idAdmin = table.Column<int>(nullable: false),
                    foto = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    nome = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    descricao = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    video = table.Column<string>(unicode: false, maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Exercici__5330B3C4556A8F6A", x => x.idExercicio);
                    table.ForeignKey(
                        name: "FK__Exercicio__idAdm__5629CD9C",
                        column: x => x.idAdmin,
                        principalTable: "Administrador",
                        principalColumn: "idAdmin",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Utilizador",
                columns: table => new
                {
                    idUtilizador = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    idAdmin = table.Column<int>(nullable: false),
                    numTelefone = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    nome = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    fotografica = table.Column<string>(unicode: false, maxLength: 256, nullable: false),
                    estado = table.Column<int>(nullable: false),
                    sexo = table.Column<string>(unicode: false, maxLength: 25, nullable: false),
                    email = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    palavraPass = table.Column<string>(unicode: false, maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Utilizad__9F6A66A3D059DAC1", x => x.idUtilizador);
                    table.ForeignKey(
                        name: "FK__Utilizado__idAdm__73BA3083",
                        column: x => x.idAdmin,
                        principalTable: "Administrador",
                        principalColumn: "idAdmin",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Mensagem",
                columns: table => new
                {
                    idMensagem = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    utilizadorDe = table.Column<int>(nullable: false),
                    utilizadorPara = table.Column<int>(nullable: false),
                    texto = table.Column<string>(unicode: false, maxLength: 1000, nullable: false),
                    arquivada = table.Column<bool>(nullable: false),
                    lida = table.Column<bool>(nullable: false),
                    dataEnvio = table.Column<DateTime>(type: "datetime", nullable: false),
                    dataRecebido = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Mensagem__AAFB640DCE9F3493", x => x.idMensagem);
                    table.ForeignKey(
                        name: "FK__Mensagem__utiliz__04E4BC85",
                        column: x => x.utilizadorDe,
                        principalTable: "Utilizador",
                        principalColumn: "idUtilizador",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__Mensagem__utiliz__05D8E0BE",
                        column: x => x.utilizadorPara,
                        principalTable: "Utilizador",
                        principalColumn: "idUtilizador",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Professor",
                columns: table => new
                {
                    idProfessor = table.Column<int>(nullable: false),
                    idUtilizador = table.Column<int>(nullable: false),
                    especialidade = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Professo__4E7C3C6DB004F223", x => x.idProfessor);
                    table.ForeignKey(
                        name: "FK__Professor__idUti__778AC167",
                        column: x => x.idUtilizador,
                        principalTable: "Utilizador",
                        principalColumn: "idUtilizador",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Socio",
                columns: table => new
                {
                    idSocio = table.Column<int>(nullable: false),
                    idUtilizador = table.Column<int>(nullable: false),
                    altura = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Socio__E19769C1AFD53D0E", x => x.idSocio);
                    table.ForeignKey(
                        name: "FK__Socio__idUtiliza__7B5B524B",
                        column: x => x.idUtilizador,
                        principalTable: "Utilizador",
                        principalColumn: "idUtilizador",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Peso",
                columns: table => new
                {
                    idProfessor = table.Column<int>(nullable: false),
                    idSocio = table.Column<int>(nullable: false),
                    dataCriacao = table.Column<DateTime>(type: "datetime", nullable: false),
                    valor = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Peso__D8C0FE596FC9DD7A", x => new { x.idProfessor, x.idSocio, x.dataCriacao });
                    table.ForeignKey(
                        name: "FK__Peso__idProfesso__7E37BEF6",
                        column: x => x.idProfessor,
                        principalTable: "Professor",
                        principalColumn: "idProfessor",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__Peso__idSocio__7F2BE32F",
                        column: x => x.idSocio,
                        principalTable: "Socio",
                        principalColumn: "idSocio",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PlanoExercicio",
                columns: table => new
                {
                    idPlano = table.Column<int>(nullable: false),
                    idProfessor = table.Column<int>(nullable: false),
                    idSocio = table.Column<int>(nullable: false),
                    descricao = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    ativo = table.Column<bool>(nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__PlanoExe__39B860229320CE31", x => x.idPlano);
                    table.ForeignKey(
                        name: "FK__PlanoExer__idPro__0F624AF8",
                        column: x => x.idProfessor,
                        principalTable: "Professor",
                        principalColumn: "idProfessor",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__PlanoExer__idSoc__10566F31",
                        column: x => x.idSocio,
                        principalTable: "Socio",
                        principalColumn: "idSocio",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Exercico_Plano",
                columns: table => new
                {
                    idExercicio = table.Column<int>(nullable: false),
                    idPlano = table.Column<int>(nullable: false),
                    ordem = table.Column<int>(nullable: false),
                    duracaoPeriodoDescanso = table.Column<string>(unicode: false, maxLength: 15, nullable: false),
                    quantidadeSeries = table.Column<int>(nullable: false),
                    numeroRepeticoes = table.Column<int>(nullable: false),
                    duracaoExercicios = table.Column<string>(unicode: false, maxLength: 15, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Exercico__5D8A6D2B6503DCF7", x => new { x.idExercicio, x.idPlano, x.ordem });
                    table.ForeignKey(
                        name: "FK__Exercico___idExe__17036CC0",
                        column: x => x.idExercicio,
                        principalTable: "ExercicioGinasio",
                        principalColumn: "idExercicio",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__Exercico___idPla__17F790F9",
                        column: x => x.idPlano,
                        principalTable: "PlanoExercicio",
                        principalColumn: "idPlano",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AulaGrupo_idAdmin",
                table: "AulaGrupo",
                column: "idAdmin");

            migrationBuilder.CreateIndex(
                name: "IX_ExercicioGinasio_idAdmin",
                table: "ExercicioGinasio",
                column: "idAdmin");

            migrationBuilder.CreateIndex(
                name: "IX_Exercico_Plano_idPlano",
                table: "Exercico_Plano",
                column: "idPlano");

            migrationBuilder.CreateIndex(
                name: "IX_Mensagem_utilizadorDe",
                table: "Mensagem",
                column: "utilizadorDe");

            migrationBuilder.CreateIndex(
                name: "IX_Mensagem_utilizadorPara",
                table: "Mensagem",
                column: "utilizadorPara");

            migrationBuilder.CreateIndex(
                name: "IX_Peso_idSocio",
                table: "Peso",
                column: "idSocio");

            migrationBuilder.CreateIndex(
                name: "IX_PlanoExercicio_idProfessor",
                table: "PlanoExercicio",
                column: "idProfessor");

            migrationBuilder.CreateIndex(
                name: "IX_PlanoExercicio_idSocio",
                table: "PlanoExercicio",
                column: "idSocio");

            migrationBuilder.CreateIndex(
                name: "UQ__Professo__9F6A66A2C11C0D2E",
                table: "Professor",
                column: "idUtilizador",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "UQ__Socio__E19769C080D37256",
                table: "Socio",
                column: "idSocio",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Socio_idUtilizador",
                table: "Socio",
                column: "idUtilizador");

            migrationBuilder.CreateIndex(
                name: "UQ__Utilizad__AB6E6164FF1F4950",
                table: "Utilizador",
                column: "email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Utilizador_idAdmin",
                table: "Utilizador",
                column: "idAdmin");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AulaGrupo");

            migrationBuilder.DropTable(
                name: "Exercico_Plano");

            migrationBuilder.DropTable(
                name: "Ginasio");

            migrationBuilder.DropTable(
                name: "Mensagem");

            migrationBuilder.DropTable(
                name: "Peso");

            migrationBuilder.DropTable(
                name: "ExercicioGinasio");

            migrationBuilder.DropTable(
                name: "PlanoExercicio");

            migrationBuilder.DropTable(
                name: "Professor");

            migrationBuilder.DropTable(
                name: "Socio");

            migrationBuilder.DropTable(
                name: "Utilizador");

            migrationBuilder.DropTable(
                name: "Administrador");
        }
    }
}
