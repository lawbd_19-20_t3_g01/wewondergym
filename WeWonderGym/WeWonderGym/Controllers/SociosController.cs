﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Hosting;
using WeWonderGym.Data;
using WeWonderGym.Models;
using Microsoft.EntityFrameworkCore;
using WeWonderGym.Filters;

namespace WeWonderGym.Controllers
{
    [RoleFilter(Perfil = "Socio")]
    public class SociosController : Controller
    {
        private readonly WeWonderGymDataBaseContext _context;
        private readonly IHostEnvironment _hostEnvironment;

        public SociosController(WeWonderGymDataBaseContext context, IHostEnvironment hostEnvironment)
        {
            _context = context;
            _hostEnvironment = hostEnvironment;
        }

        // GET: Socios
        public async Task<IActionResult> Index()
        {
            var weWonderGymDataBaseContext = _context.Socio.Include(s => s.IdUtilizadorNavigation);
            return View(await weWonderGymDataBaseContext.ToListAsync());
        }

        // GET: Socios/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var socio = await _context.Socio
                .Include(s => s.IdUtilizadorNavigation)
                .FirstOrDefaultAsync(m => m.IdSocio == id);
            if (socio == null)
            {
                return NotFound();
            }

            return View(socio);
        }

        // GET: Socios/Create
        public IActionResult Create()
        {
            ViewData["IdUtilizador"] = new SelectList(_context.Utilizador, "IdUtilizador", "Email");
            return View();
        }

        // POST: Socios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdUtilizador,IdSocio,Altura")] Socio socio)
        {
            if (ModelState.IsValid)
            {
                _context.Add(socio);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdUtilizador"] = new SelectList(_context.Utilizador, "IdUtilizador", "Email", socio.IdUtilizador);
            return View(socio);
        }

        // GET: Socios/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var socio = await _context.Socio.FindAsync(id);
            if (socio == null)
            {
                return NotFound();
            }
            ViewData["IdUtilizador"] = new SelectList(_context.Utilizador, "IdUtilizador", "Email", socio.IdUtilizador);
            return View(socio);
        }

        // POST: Socios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdUtilizador,IdSocio,Altura")] Socio socio)
        {
            if (id != socio.IdSocio)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(socio);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SocioExists(socio.IdSocio))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdUtilizador"] = new SelectList(_context.Utilizador, "IdUtilizador", "Email", socio.IdUtilizador);
            return View(socio);
        }

        // GET: Socios/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var socio = await _context.Socio
                .Include(s => s.IdUtilizadorNavigation)
                .FirstOrDefaultAsync(m => m.IdSocio == id);
            if (socio == null)
            {
                return NotFound();
            }

            return View(socio);
        }

        // POST: Socios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var socio = await _context.Socio.FindAsync(id);
            _context.Socio.Remove(socio);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SocioExists(int id)
        {
            return _context.Socio.Any(e => e.IdSocio == id);
        }

        //  ################ ZONA AULA DE GRUPO ###############################
        public async Task<IActionResult> IndexAulaGrupo()
        {
            var aulas = _context.Aula.Include(x => x.IdAulaGrupoNavigation).Include(x => x.IdAdminNavigation).Include(x => x.IdProfessorNavigation.IdUtilizadorNavigation);
            Dictionary<int, int> aux = new Dictionary<int, int>();
            int idUtilizador = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            Socio ss = _context.Socio.First(x => x.IdUtilizador == idUtilizador);

            ViewData["Inscricao"] = dictGeneratorInscricao(ss.IdSocio);
            ViewData["Vagas"] = dictGeneratorVagas();
            return View(await aulas.ToListAsync());
        }

        public async Task<IActionResult> InscreverAula(int id)
        {
            int idUtilizador = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            Socio ss = _context.Socio.First(x => x.IdUtilizador == idUtilizador);

            Inscrever inscrever = new Inscrever();
            inscrever.IdAula = id;
            inscrever.IdSocio = ss.IdSocio;
            inscrever.IdSocioNavigation = ss;
            inscrever.DataAula = DateTime.Now;

            _context.Inscrever.Add(inscrever);
            await _context.SaveChangesAsync();

            var aulas = _context.Aula.Include(x => x.IdAulaGrupoNavigation).Include(x => x.IdAdminNavigation).Include(x => x.IdProfessorNavigation.IdUtilizadorNavigation);

            ViewData["Inscricao"] = dictGeneratorInscricao(ss.IdSocio);
            ViewData["Vagas"] = dictGeneratorVagas();
            return PartialView("InscreverAulaPartial", aulas);
        }

        public Dictionary<int, int> dictGeneratorInscricao(int idSocio)
        {

            Dictionary<int, int> aux = new Dictionary<int, int>();

            var listaAux = _context.Aula.Select(a =>
                           new SelectListItem
                           {
                               Value = a.IdAula.ToString(),
                               Text = _context.Inscrever.Where(x => x.IdSocio == idSocio && x.IdAula == a.IdAula).ToList().Count.ToString(),
                           }).ToList();
            foreach (var item in listaAux)
            {
                aux[Convert.ToInt32(item.Value)] = Convert.ToInt32(item.Text);
            }

            return aux;
        }
        public Dictionary<int, int> dictGeneratorVagas()
        {

            Dictionary<int, int> aux = new Dictionary<int, int>();

            var listaAux = _context.Aula.Select(a =>
                           new SelectListItem
                           {
                               Value = a.IdAula.ToString(),
                               Text = (a.Lotacao - _context.Inscrever.Where(x => x.IdAula == a.IdAula).ToList().Count).ToString(),
                           }).ToList();

            foreach (var item in listaAux)
            {
                aux[Convert.ToInt32(item.Value)] = Convert.ToInt32(item.Text);
            }

            return aux;
        }

        // ################# PERSONAL TRAINER #####################################
        public IActionResult IndexPersonalTrainer()
        {
            return View();
        }

        public async Task<IActionResult> ListProfessor()
        {
            int idUtilizador = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            Socio ss = _context.Socio.First(x => x.IdUtilizador == idUtilizador);
            var weWonderGymDataBaseContext = _context.PedidoPt.Include(p => p.IdProfessorNavigation.IdUtilizadorNavigation);
            ViewData["Pedido"] = _context.PedidoPt.Any(x => x.IdSocio == ss.IdSocio && x.Estado != 0);
            ViewData["Professores"] = _context.Professor.Include(x => x.IdUtilizadorNavigation);
            ViewData["IdSocio"] = ss.IdSocio;
            return View(await weWonderGymDataBaseContext.ToListAsync());
        }

        public async Task<IActionResult> RequisitarPT(int id)
        {
            int idUtilizador = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            Socio ss = _context.Socio.First(x => x.IdUtilizador == idUtilizador);
            PedidoPt pedido = new PedidoPt();

            try
            {
                pedido.DataPedido = DateTime.Now;
                pedido.IdAdmin = 1;
                pedido.IdProfessor = id;
                pedido.IdSocio = ss.IdSocio;
                pedido.Estado = 2;
                _context.PedidoPt.Add(pedido);

                await _context.SaveChangesAsync();
            }catch(Exception e)
            {

            }
       
            var weWonderGymDataBaseContext = _context.PedidoPt.Include(p => p.IdProfessorNavigation.IdUtilizadorNavigation);
            ViewData["Pedido"] = _context.PedidoPt.Any(x => x.IdSocio == ss.IdSocio && x.Estado != 0);
            ViewData["IdSocio"] = ss.IdSocio;
            ViewData["Professores"] = _context.Professor.Include(x => x.IdUtilizadorNavigation);
            return PartialView("ListPartialPersonalTrainer", await weWonderGymDataBaseContext.ToListAsync());

        }
        public IActionResult PerfilPT()
        {
            int idUtilizador = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            Socio ss = _context.Socio.First(x => x.IdUtilizador == idUtilizador);
            var professor = _context.PedidoPt.Include(x => x.IdProfessorNavigation.IdUtilizadorNavigation).FirstOrDefault(x => x.IdSocio == ss.IdSocio && x.Estado == 1);

            return View(professor);
        }

        // ########################### PLANO DE TREINO #######################
        public IActionResult ListPlano()
        {
            int idUtilizador = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            Socio ss = _context.Socio.First(x => x.IdUtilizador == idUtilizador);
            var exercicios = _context.ExercicoPlano.Include(e => e.IdExercicioNavigation);
            ViewData["Exercicios"] = exercicios;

            Dictionary<int, string> aux = new Dictionary<int, string>();

            var listaAux = _context.ExercicoPlano.Select(a =>
                           new SelectListItem
                           {
                               Value = a.IdExercicio.ToString(),
                               Text = _context.ExercicioGinasio.FirstOrDefault(x => x.IdExercicio == a.IdExercicio).Nome.ToString(),
                           }).ToList();

            foreach (var item in listaAux)
            {
                aux[Convert.ToInt32(item.Value)] = item.Text;
            }
            ViewData["NomePlanos"] = aux;
            var planos = _context.PlanoExercicio.Include(x => x.IdProfessorNavigation.IdUtilizadorNavigation).Include(x => x.ExercicoPlano).Where(x => x.IdSocio == ss.IdSocio && x.Ativo == true);
            return View(planos);
        }
        public IActionResult DetalhesPlano(int id)
        {
            ExercicioGinasio exerc = _context.ExercicioGinasio.FirstOrDefault(x => x.IdExercicio == id);
            return PartialView("DetalhesPlano", exerc);
        }

        public async Task<IActionResult> Historico()
        {
            int idUtilizador = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            Socio ss = _context.Socio.First(x => x.IdUtilizador == idUtilizador);
            ViewData["socio"] = _context.Socio.Include(x => x.IdUtilizadorNavigation).FirstOrDefault(x => x.IdSocio == ss.IdSocio);
            var weWonderGymDataBaseContext = _context.Peso.Include(s => s.IdProfessorNavigation.IdUtilizadorNavigation).Where(x => x.IdSocio == ss.IdSocio);
            return View(await weWonderGymDataBaseContext.ToListAsync());
        }

        [HttpGet]
        public ActionResult AlterarProfile()
        {
            var utilizador = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == Convert.ToInt32(HttpContext.Session.GetInt32("UserId")));
            return View(utilizador);
        }

        [HttpPost]
        public async Task<IActionResult> AlterarProfile(IFormCollection colletion, [Bind("NumTelefone,Fotografica,PalavraPass")] Utilizador utilizador, string OldPass)
        {
            Utilizador edit = _context.Utilizador.Find(Convert.ToInt32(HttpContext.Session.GetInt32("UserId")));
            SHA512 sha512 = SHA512Managed.Create();

            if (utilizador.PalavraPass != null)
            {
                byte[] bytes = sha512.ComputeHash(Encoding.UTF8.GetBytes(utilizador.PalavraPass), 0, Encoding.UTF8.GetByteCount(utilizador.PalavraPass));
                string passHash = Convert.ToBase64String(bytes);
                utilizador.PalavraPass = passHash;
            }
            if (OldPass != null)
            {
                byte[] bytesOldPass = sha512.ComputeHash(Encoding.UTF8.GetBytes(OldPass), 0, Encoding.UTF8.GetByteCount(OldPass));
                OldPass = Convert.ToBase64String(bytesOldPass);
            }


            if ((utilizador.PalavraPass != null && OldPass != null))
            {
                if (edit.PalavraPass != OldPass)
                    ModelState.AddModelError("OldPass", "Password incorreta!");
            }

            if ((utilizador.PalavraPass == null && OldPass != null) || (utilizador.PalavraPass != null && OldPass == null))
            {
                ModelState.AddModelError("OldPass", "Tem que preencher ambos os campos se deseja mudar a Password");
            }
            var utl = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == Convert.ToInt32(HttpContext.Session.GetInt32("UserId")));
            if (ModelState.IsValid)
            {
                try
                {
                    var file = colletion.Files.First();
                    if (file.ContentType != null)
                    {
                        string caminho = Path.Combine(_hostEnvironment.ContentRootPath, "wwwroot\\img\\Fotos\\Utilizador");
                        string nome_ficheiro = Path.GetFileName(file.FileName);
                        string caminho_completo = Path.Combine(caminho, nome_ficheiro);
                        FileStream fs = new FileStream(caminho_completo, FileMode.Create);
                        file.CopyTo(fs);
                        fs.Close();


                        string caminho_deleteC = Path.Combine(caminho, edit.Fotografica);


                        System.IO.File.Delete(caminho_deleteC);

                        edit.Fotografica = nome_ficheiro;
                    }

                    edit.NumTelefone = utilizador.NumTelefone;

                    if (utilizador.PalavraPass != null)
                        edit.PalavraPass = utilizador.PalavraPass;

                    _context.Update(edit);
                    await _context.SaveChangesAsync();
                    ViewData["Sucesso"] = "sucesso";
                    return PartialView(utl);
                }
                catch (Exception)
                {
                    edit.NumTelefone = utilizador.NumTelefone;

                    if (utilizador.PalavraPass != null)
                        edit.PalavraPass = utilizador.PalavraPass;

                    _context.Update(edit);
                    await _context.SaveChangesAsync();
                    ViewData["Sucesso"] = "sucesso";
                    return PartialView(utl);
                }
            }
            return PartialView(utl);
        }
    }
}
