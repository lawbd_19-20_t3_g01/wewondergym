﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Hosting;
using WeWonderGym.Data;
using WeWonderGym.Models;

namespace WeWonderGym.Controllers
{
    public class UtilizadoresController : Controller
    {
        private WeWonderGymDataBaseContext _context;
        private readonly IHostEnvironment _hostEnvironment;

        public UtilizadoresController(WeWonderGymDataBaseContext contextoDB, IHostEnvironment hostEnvironment)
        {
            _context = contextoDB;
            _hostEnvironment = hostEnvironment;
        }

        public IActionResult Login(string ReturnUrl)
        {
            return PartialView("Login");
        }

        [HttpPost]
        public IActionResult Login(string Email, string Password, string? ReturnUrl, [Bind("Email,Password")] LoginUtilizador utilizador)
        {
            try
            {
                SHA512 sha512 = SHA512Managed.Create();
                byte[] bytes = sha512.ComputeHash(Encoding.UTF8.GetBytes(Password), 0, Encoding.UTF8.GetByteCount(Password));
                string passHash = Convert.ToBase64String(bytes);
                Utilizador uu = _context.Utilizador.SingleOrDefault(u => u.Email == Email);


                if (uu != null)
                {

                    if(uu.Estado == 3)
                    {
                        return RedirectToAction("ConfirmSocio",utilizador);
                    }
                    else if (uu.Estado == 0)
                    {
                        ModelState.AddModelError("Email", _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == uu.IdUtilizador).EstadoMotivo.ToString());
                        return PartialView(utilizador);
                    }

                    Utilizador u = _context.Utilizador.SingleOrDefault(u => u.Email == Email && u.PalavraPass == passHash);
                    if(u != null)
                    {
                        HttpContext.Session.SetString("Email", u.Email);
                        HttpContext.Session.SetInt32("UserId", u.IdUtilizador);

                        if (_context.Socio.FirstOrDefault(x => x.IdUtilizador == u.IdUtilizador) != null)
                        {
                            HttpContext.Session.SetString("Perfil", "Socio");
                            ViewData["Perfil"] = "Socio";
                            ViewData["sucess"] = "true";
                            return PartialView();
                        }

                        if (_context.Professor.FirstOrDefault(x => x.IdUtilizador == u.IdUtilizador) != null)
                        {
                            HttpContext.Session.SetString("Perfil", "Professor");
                            ViewData["Perfil"] = "Professor";
                            ViewData["sucess"] = "true";
                            return PartialView();
                        }
                        else
                        {
                            ModelState.AddModelError("Password","Password Incorreta!");
                            return PartialView(utilizador);
                        }
                    }
                 
                }

                Administrador aa = _context.Administrador.SingleOrDefault(x => x.UserName == Email && x.PalavraPass == passHash);

                if (aa != null)
                {
                    HttpContext.Session.SetString("Email", aa.UserName);
                    HttpContext.Session.SetInt32("UserId", aa.IdAdmin);
                    HttpContext.Session.SetString("Perfil", "Admin"); 
                    ViewData["sucess"] = "true";
                    return PartialView();
                }

                ModelState.AddModelError("Email", "O email não foi encontrado");
                return PartialView(utilizador);

            }
            catch (Exception e)
            {
                ModelState.AddModelError("Email", "Um erro inesperado aconteceu, volte a tentar.");
                return PartialView(utilizador);
            }
        }

        public IActionResult ConfirmSocio(LoginUtilizador u)
        {
          
            return PartialView(u);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ConfirmSocio(string email,string pass, string Repass)
        {
            if(pass != Repass)
            {
                ViewBag.Sucesso = "erro";
                LoginUtilizador u = new LoginUtilizador();
                u.Email = email;
                return PartialView(u);
            }

            SHA512 sha512 = SHA512Managed.Create();
            byte[] bytes = sha512.ComputeHash(Encoding.UTF8.GetBytes(pass), 0, Encoding.UTF8.GetByteCount(pass));
            string passHash = Convert.ToBase64String(bytes);


            var util = _context.Utilizador.SingleOrDefault(x => x.Email == email);
            util.PalavraPass = passHash;
            util.Estado = 1;
            _context.Utilizador.Update(util);
            _context.SaveChanges();

            return RedirectToAction("Login");
        }

        public IActionResult Logout()
        {
            HttpContext.Response.Cookies.Delete("CookieSessao");
            return LocalRedirect("/");
        }

        public static bool isSocio(HttpContext contexto)
        {
            if (contexto.Session.GetString("Perfil") == "Socio")
                return true;
            else
                return false;
        }

        public static bool isProfessor(HttpContext contexto)
        {
            if (contexto.Session.GetString("Perfil") == "Professor")
                return true;
            else
                return false;
        }

        public static bool isAdmin(HttpContext contexto)
        {
            if (contexto.Session.GetString("Perfil") == "Admin")
                return true;
            else
                return false;
        }

        public static bool estaAutenticado(HttpContext contexto)
        {
            if (contexto.Session.GetInt32("UserId") != null)
                return true;
            else
                return false;
        }


        // GET: Utilizadors
        public async Task<IActionResult> Index()
        {
            var weWonderGymDataBaseContext = _context.Utilizador.Include(u => u.IdAdminNavigation);
            return View(await weWonderGymDataBaseContext.ToListAsync());
        }

        // GET: Utilizadors/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var utilizador = await _context.Utilizador
                .Include(u => u.IdAdminNavigation)
                .FirstOrDefaultAsync(m => m.IdUtilizador == id);
            if (utilizador == null)
            {
                return NotFound();
            }

            return View(utilizador);
        }

        // GET: Utilizadors/Create
        public IActionResult Create()
        {
            return PartialView("Create");
        }

        // POST: Utilizadors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(IFormCollection colletion, [Bind("IdUtilizador,IdAdmin,NumTelefone,Nome,Fotografica,Estado,Sexo,Email,PalavraPass")] Utilizador utilizador)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    utilizador.PalavraPass = RandomString(7);
                    SHA512 sha512 = SHA512Managed.Create();
                    byte[] bytes = sha512.ComputeHash(Encoding.UTF8.GetBytes(utilizador.PalavraPass), 0, Encoding.UTF8.GetByteCount(utilizador.PalavraPass));
                    string passHash = Convert.ToBase64String(bytes);
                    utilizador.PalavraPass = passHash;
                    utilizador.Estado = 2;
                    utilizador.IdAdmin = 1;
                    utilizador.EstadoMotivo = "A sua conta ainda não foi ativada!";

                    IFormFile file = colletion.Files.First();
                    string caminho = Path.Combine(_hostEnvironment.ContentRootPath, "wwwroot\\img\\Fotos\\Utilizador");
                    string nome_ficheiro = Path.GetFileName(file.FileName);
                    string caminho_completo = Path.Combine(caminho, nome_ficheiro);
                    FileStream fs = new FileStream(caminho_completo, FileMode.Create);
                    file.CopyTo(fs);
                    fs.Close();

                    utilizador.Fotografica = nome_ficheiro;
                    _context.Add(utilizador);
                    await _context.SaveChangesAsync();

                    var ss = new Socio();
                    ss.Altura = colletion["Altura"];
                    Utilizador utl = _context.Utilizador.First(x => x.Email == utilizador.Email);
                    ss.IdUtilizador = utl.IdUtilizador;
                    ss.IdSocio = utl.IdUtilizador + 2;
                    _context.Add(ss);
                    await _context.SaveChangesAsync();
                    ViewData["Sucess"] = "sucesso";
                    return PartialView();
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("Email", "Email já em uso!");
                    return PartialView(utilizador);
                }

            }
            ViewData["IdAdmin"] = new SelectList(_context.Administrador, "IdAdmin", "Username", utilizador.IdAdmin);
            return PartialView(utilizador);
        }

        // GET: Utilizadors/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var utilizador = await _context.Utilizador.FindAsync(id);
            if (utilizador == null)
            {
                return NotFound();
            }
            ViewData["IdAdmin"] = new SelectList(_context.Administrador, "IdAdmin", "PalavraPass", utilizador.IdAdmin);
            return View(utilizador);
        }

        // POST: Utilizadors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdUtilizador,IdAdmin,NumTelefone,Nome,Fotografica,Estado,Sexo,Email,PalavraPass")] Utilizador utilizador)
        {
            if (id != utilizador.IdUtilizador)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(utilizador);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UtilizadorExists(utilizador.IdUtilizador))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdAdmin"] = new SelectList(_context.Administrador, "IdAdmin", "PalavraPass", utilizador.IdAdmin);
            return View(utilizador);
        }

        // GET: Utilizadors/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var utilizador = await _context.Utilizador
                .Include(u => u.IdAdminNavigation)
                .FirstOrDefaultAsync(m => m.IdUtilizador == id);
            if (utilizador == null)
            {
                return NotFound();
            }

            return View(utilizador);
        }

        // POST: Utilizadors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var utilizador = await _context.Utilizador.FindAsync(id);
            _context.Utilizador.Remove(utilizador);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UtilizadorExists(int id)
        {
            return _context.Utilizador.Any(e => e.IdUtilizador == id);
        }

        public IActionResult ForgotPass()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ForgotPass(string email)
        {
            var user = _context.Utilizador.Where(x => x.Email == email);

            if (user.Count() == 0)
            {
                ModelState.AddModelError("Erro", "O email inserido não está registado, verifique e tente novamente.");
                ViewBag.Sucesso = "erro";
                return PartialView();
            }
          if(ModelState.IsValid)
            {
                string newPass = RandomString(7);

                SHA512 sha512 = SHA512Managed.Create();
                byte[] bytes = sha512.ComputeHash(Encoding.UTF8.GetBytes(newPass), 0, Encoding.UTF8.GetByteCount(newPass));
                string passHash = Convert.ToBase64String(bytes);
                Utilizador u = _context.Utilizador.SingleOrDefault(u => u.Email == email);
                u.PalavraPass = passHash;
                _context.Update(u);
                _context.SaveChanges();


                try
                {
                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                    mail.From = new MailAddress("wewondergym123@gmail.com");
                    mail.To.Add(email);
                    mail.Subject = "Recuperação da Password";
                    mail.Body = "A sua nova password é: " + newPass + System.Environment.NewLine + "Para alterar a sua password dirija-se ao perfil e altere a mesma";

                    SmtpServer.Port = 587;
                    SmtpServer.Credentials = new System.Net.NetworkCredential("wewondergym123@gmail.com", "WeWonder_123");
                    SmtpServer.EnableSsl = true;

                    SmtpServer.Send(mail);

                }
                catch (Exception ex)
                {

                }
            }
            return RedirectToAction("Login");
        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public IActionResult MenssagemIndex()
        {
            int idUtilizador = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            Utilizador uu = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == idUtilizador);
            ajustarMensagemPara(idUtilizador);
            var mensagens = _context.Mensagem.Where(x => x.UtilizadorDe == idUtilizador || x.UtilizadorPara == idUtilizador).Include(x => x.UtilizadorDeNavigation).Include(x => x.UtilizadorParaNavigation).ToList();
            foreach (var k in mensagens)
            {
                k.UtilizadorDeNavigation = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == k.UtilizadorDe);
                k.UtilizadorParaNavigation = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == k.UtilizadorPara);
            }
            ViewData["UserEmail"] = uu.Email;
            return PartialView(mensagens);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EnviarMensagem([Bind("IdMensagem,UtilizadorDe,UtilizadorPara,Texto,Arquivada,Lida,DataEnvio,DataRecebido")] Mensagem mensagem)
        {
            int idUtilizador = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            if (ModelState.IsValid)
            {
                
                mensagem.DataEnvio = DateTime.Now;
                mensagem.DataRecebido = DateTime.Now;
                mensagem.UtilizadorDe = idUtilizador;
                mensagem.UtilizadorParaNavigation = _context.Utilizador.FirstOrDefault(x=> x.IdUtilizador == mensagem.UtilizadorPara);
                mensagem.UtilizadorDeNavigation = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == idUtilizador);

                _context.Add(mensagem);
                await _context.SaveChangesAsync();
                ViewData["sucess"] = "yes";

                List<Mensagem> lista = _context.Mensagem.Where(x => x.UtilizadorDe == idUtilizador || x.UtilizadorPara == idUtilizador).Include(x => x.UtilizadorDeNavigation).Include(x => x.UtilizadorParaNavigation).ToList();
                foreach (var k in lista)
                {
                    k.UtilizadorDeNavigation = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == k.UtilizadorDe);
                    k.UtilizadorParaNavigation = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == k.UtilizadorPara);
                }
                ajustarMensagemPara(idUtilizador);
                ViewData["UserEmail"] = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == idUtilizador).Email;
                return PartialView("MenssagemIndex",lista);
            }


            ajustarMensagemPara(idUtilizador);
            ViewData["sucess"] = "not";
            var mensagens = _context.Mensagem.Where(x => x.UtilizadorDe == idUtilizador || x.UtilizadorPara == idUtilizador).Include(x => x.UtilizadorDeNavigation).Include(x => x.UtilizadorParaNavigation).ToList();
            foreach (var k in mensagens)
            {
                k.UtilizadorDeNavigation = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == k.UtilizadorDe);
                k.UtilizadorParaNavigation = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == k.UtilizadorPara);
            }
            ViewData["UserEmail"] = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == idUtilizador).Email;
            return PartialView(mensagens);
        }

        public IActionResult ShowMensagemPartial(int id)
        {
            int idUtilizador = Convert.ToInt32(HttpContext.Session.GetInt32("UserId")); ;
            var mensagem = _context.Mensagem.FirstOrDefault(x => x.IdMensagem == id);
            if(mensagem.UtilizadorPara == idUtilizador && !mensagem.Lida)
            {
                mensagem.Lida = true;
                _context.Update(mensagem);
                _context.SaveChanges();
            }
            mensagem.UtilizadorDeNavigation = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == mensagem.UtilizadorDe);
            mensagem.UtilizadorParaNavigation = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == mensagem.UtilizadorPara);
            return PartialView("ShowMensagemPartial", mensagem);
        }
        public IActionResult ArquivarMensagem(int id)
        {
            int idUtilizador = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            var mensagem = _context.Mensagem.FirstOrDefault(x => x.IdMensagem == id);
            mensagem.Arquivada = true;
            _context.Update(mensagem);
            _context.SaveChanges();
            var mensagens = _context.Mensagem.Where(x => x.UtilizadorDe == idUtilizador || x.UtilizadorPara == idUtilizador).Include(x => x.UtilizadorDeNavigation).Include(x => x.UtilizadorParaNavigation).ToList();
            foreach (var k in mensagens)
            {
                k.UtilizadorDeNavigation = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == k.UtilizadorDe);
                k.UtilizadorParaNavigation = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == k.UtilizadorPara);
            }
            ajustarMensagemPara(idUtilizador);
            ViewData["UserEmail"] = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == idUtilizador).Email;
            return PartialView("MenssagemIndex",mensagens);
        }

        public void ajustarMensagemPara(int idUtilizador)
        {
            if (HttpContext.Session.GetString("Perfil") == "Socio")
            {

                Socio ss = _context.Socio.FirstOrDefault(x => x.IdUtilizador == idUtilizador);
                ViewData["UtilizadorPara"] = _context.PedidoPt.Where(x => x.IdSocio == ss.IdSocio && x.Estado == 1).Include(x => x.IdProfessorNavigation.IdUtilizadorNavigation)
                    .Include(x => x.IdSocioNavigation.IdUtilizador)
                    .Select(a =>
                             new SelectListItem
                             {
                                 Value = a.IdProfessorNavigation.IdUtilizadorNavigation.IdUtilizador.ToString(),
                                 Text = a.IdProfessorNavigation.IdUtilizadorNavigation.Nome,
                             }).ToList();

            }
            else if (HttpContext.Session.GetString("Perfil") == "Professor")
            {
                Professor pp = _context.Professor.FirstOrDefault(x => x.IdUtilizador == idUtilizador);
                ViewData["UtilizadorPara"] = _context.PedidoPt.Where(x => x.IdProfessor == pp.IdProfessor && x.Estado == 1).Include(x => x.IdProfessorNavigation.IdUtilizadorNavigation)
                .Include(x => x.IdSocioNavigation.IdUtilizador)
                .Select(a =>
                         new SelectListItem
                         {
                             Value = a.IdSocioNavigation.IdUtilizadorNavigation.IdUtilizador.ToString(),
                             Text = a.IdSocioNavigation.IdUtilizadorNavigation.Email,
                         }).ToList();

            }
        }

    }
}