﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using WeWonderGym.Data;
using WeWonderGym.Filters;
using WeWonderGym.Models;

namespace WeWonderGym.Controllers
{
    [RoleFilter(Perfil = "Admin")]
    public class AdministradorsController : Controller
    {
        private readonly WeWonderGymDataBaseContext _context;
        private readonly IHostEnvironment _hostEnvironment;

        public AdministradorsController(WeWonderGymDataBaseContext context, IHostEnvironment hostEnvironment)
        {
            _context = context;
            _hostEnvironment = hostEnvironment;
        }

        // GET: Administradors
        public async Task<IActionResult> Index()
        {
            return View(await _context.Administrador.ToListAsync());
        }

        // GET: Administradors/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var administrador = await _context.Administrador
                .FirstOrDefaultAsync(m => m.IdAdmin == id);
            if (administrador == null)
            {
                return NotFound();
            }

            return View(administrador);
        }

        // GET: Administradors/Create
        public IActionResult Create()
        {
            return PartialView();
        }

        // POST: Administradors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdAdmin,UserName,PalavraPass")] Administrador administrador)
        {
            if (ModelState.IsValid)
            {
                SHA512 sha512 = SHA512Managed.Create();
                byte[] bytes = sha512.ComputeHash(Encoding.UTF8.GetBytes(administrador.PalavraPass), 0, Encoding.UTF8.GetByteCount(administrador.PalavraPass));
                string passHash = Convert.ToBase64String(bytes);
                administrador.PalavraPass = passHash;
                _context.Add(administrador);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(administrador);
        }

        // GET: Administradors/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var administrador = await _context.Administrador.FindAsync(id);
            if (administrador == null)
            {
                return NotFound();
            }
            return View(administrador);
        }

        // POST: Administradors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdAdmin,UserName,PalavraPass")] Administrador administrador)
        {
            if (id != administrador.IdAdmin)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(administrador);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AdministradorExists(administrador.IdAdmin))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(administrador);
        }

        // GET: Administradors/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var administrador = await _context.Administrador
                .FirstOrDefaultAsync(m => m.IdAdmin == id);
            if (administrador == null)
            {
                return NotFound();
            }

            return View(administrador);
        }

        // POST: Administradors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var administrador = await _context.Administrador.FindAsync(id);
            _context.Administrador.Remove(administrador);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AdministradorExists(int id)
        {
            return _context.Administrador.Any(e => e.IdAdmin == id);
        }


        // ############### ZONA Professor ##########################
        public IActionResult CreateProf()
        {
            ViewData["IdAdmin"] = new SelectList(_context.Administrador, "IdAdmin", "PalavraPass");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateProf(IFormCollection colletion, [Bind("IdUtilizador,IdAdmin,NumTelefone,Nome,Fotografica,Estado,Sexo,Email,PalavraPass,IdUtilizador, IdProfessor, Especialidade")] Utilizador utilizador)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SHA512 sha512 = SHA512Managed.Create();
                    byte[] bytes = sha512.ComputeHash(Encoding.UTF8.GetBytes(utilizador.PalavraPass), 0, Encoding.UTF8.GetByteCount(utilizador.PalavraPass));
                    string passHash = Convert.ToBase64String(bytes);
                    utilizador.PalavraPass = passHash;
                    utilizador.Estado = 1;
                    utilizador.EstadoMotivo = "Professor Ativo";
                    utilizador.IdAdmin = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));


                    IFormFile file = colletion.Files.First();
                    string caminho = Path.Combine(_hostEnvironment.ContentRootPath, "wwwroot\\img\\Fotos");
                    string nome_ficheiro = Path.GetFileName(file.FileName);
                    string caminho_completo = Path.Combine(caminho, nome_ficheiro);
                    FileStream fs = new FileStream(caminho_completo, FileMode.Create);
                    file.CopyTo(fs);
                    fs.Close();

                    utilizador.Fotografica = nome_ficheiro;
                    _context.Add(utilizador);
                    await _context.SaveChangesAsync();

                    var utl = _context.Utilizador.FirstOrDefault(x => x.Email == utilizador.Email);
                    utilizador.Professor = new Professor { Especialidade = colletion["Especialidade"], IdProfessor = utl.IdUtilizador + 2 };

                    _context.Update(utilizador);
                    await _context.SaveChangesAsync();


                    return RedirectToAction("ListProfessor");
                }
            }catch(Exception e)
            {
                ViewData["emailRepetido"] = "true";
                return View();
            }
    
            ViewData["IdAdmin"] = new SelectList(_context.Administrador, "IdAdmin", "Username", utilizador.IdAdmin);
            return Redirect("ListProfessor");
        }

        public IActionResult IndexProfessor()
        {
            return View();
        }

        public async Task<IActionResult> ListProfessor()
        {
            var weWonderGymDataBaseContext = _context.Professor.Include(p => p.IdUtilizadorNavigation);
            return View(await weWonderGymDataBaseContext.ToListAsync());
        }

        public IActionResult EditProfessor(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var professor = _context.Professor.Include(p => p.IdUtilizadorNavigation).SingleOrDefault(p => p.IdProfessor == id);
            if (professor == null)
            {
                return NotFound();
            }
            ViewData["IdUtilizador"] = new SelectList(_context.Utilizador, "IdUtilizador", "Email", professor.IdUtilizador);
            return PartialView(professor);
        }

        // POST: Professors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditProfessor(int id, IFormCollection collection, [Bind("IdUtilizador,IdProfessor,Especialidade,Estado")] Professor professor)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    var utl = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == professor.IdUtilizador);
                    if (utl != null)
                    {
                        utl.Estado = Convert.ToInt32(collection["Estado"]);
                        _context.Utilizador.Update(utl);
                        await _context.SaveChangesAsync();
                    }
                    _context.Update(professor);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProfessorExists(professor.IdProfessor))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(ListProfessor));
            }
            ViewData["IdUtilizador"] = new SelectList(_context.Utilizador, "IdUtilizador", "Email", professor.IdUtilizador);
            return View(professor);
        }

        private bool ProfessorExists(int id)
        {
            return _context.Professor.Any(e => e.IdProfessor == id);
        }
        public IActionResult ptProfessor()
        {
            var pedidos = _context.PedidoPt.Include(x => x.IdSocioNavigation.IdUtilizadorNavigation).Include(x => x.IdProfessorNavigation.IdUtilizadorNavigation);
            return View(pedidos);
        }

        public IActionResult AceitarPT(int id, int idSocio)
        {
            if (_context.PedidoPt.Any(x => x.IdSocio == idSocio && x.Estado == 1) == false)
            {
                var pedido = _context.PedidoPt.FirstOrDefault(x => x.IdPedido == id);
                pedido.Estado = 1;
                _context.Update(pedido);
                _context.SaveChanges();
            }
            else
            {
                ViewData["status"] = "not";
            }
            var pedidos = _context.PedidoPt.Include(x => x.IdSocioNavigation.IdUtilizadorNavigation).Include(x => x.IdProfessorNavigation.IdUtilizadorNavigation);
            return PartialView("ListPtPartial", pedidos);
        }

        public IActionResult RejeitarPT(int id, int idSocio)
        {

            var pedido = _context.PedidoPt.FirstOrDefault(x => x.IdPedido == id);
            pedido.Estado = 0;
            _context.Update(pedido);
            _context.SaveChanges();

            var pedidos = _context.PedidoPt.Include(x => x.IdSocioNavigation.IdUtilizadorNavigation).Include(x => x.IdProfessorNavigation.IdUtilizadorNavigation);
            return PartialView("ListPtPartial", pedidos);
        }

        // ############### ZONA Ginasio ##########################

        public IActionResult IndexGinasio()
        {
            var gym = _context.Ginasio.FirstOrDefault(x => x.IdGinasio == 0);
            return View(gym);
        }

        // POST: Professors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> IndexGinasio(int id, [Bind("IdGinasio,DadosLocalizacaoGps,Endereco,Email,NumTelefone,NumTelemovel")] Ginasio ginasio)
        {
            if (id != ginasio.IdGinasio)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    ginasio.Semana = "1";
                    _context.Update(ginasio);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GinasioExists(ginasio.IdGinasio))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(IndexGinasio));
            }
            return View();
        }
        private bool GinasioExists(int id)
        {
            return _context.Ginasio.Any(e => e.IdGinasio == id);
        }


        // ############### ZONA Aulas de grupo ##########################

        public IActionResult IndexAulas()
        {
            return View();
        }

        public IActionResult CreateAulas()
        {
            ViewData["IdAdmin"] = new SelectList(_context.Administrador, "IdAdmin", "PalavraPass");
            return View();
        }

        // POST: AulaGrupoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateAulas(IFormCollection colletion, [Bind("IdAulaGrupo,Foto,Nome,Descricao")] AulaGrupo aulaGrupo)
        {
            if (ModelState.IsValid)
            {
                IFormFile file = colletion.Files.First();
                string caminho = Path.Combine(_hostEnvironment.ContentRootPath, "wwwroot\\img\\FotosAulas");
                string nome_ficheiro = Path.GetFileName(file.FileName);
                string caminho_completo = Path.Combine(caminho, nome_ficheiro);
                FileStream fs = new FileStream(caminho_completo, FileMode.Create);
                file.CopyTo(fs);
                fs.Close();

                aulaGrupo.Foto = nome_ficheiro;
                int idAdmin = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                aulaGrupo.IdAdmin = idAdmin;
                aulaGrupo.IdAdminNavigation = _context.Administrador.FirstOrDefault(x => x.IdAdmin == idAdmin);
                _context.Add(aulaGrupo);
                await _context.SaveChangesAsync();
                return RedirectToAction("ListAulas");
            }
            ViewData["IdAdmin"] = new SelectList(_context.Administrador, "IdAdmin", "PalavraPass", aulaGrupo.IdAdmin);
            return View(aulaGrupo);
        }

        public async Task<IActionResult> ListAulas()
        {
            return View(await _context.AulaGrupo.ToListAsync());
        }

        public async Task<IActionResult> EditAula(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aulaGrupo = await _context.AulaGrupo.FindAsync(id);
            if (aulaGrupo == null)
            {
                return NotFound();
            }
            ViewData["OldFoto"] = aulaGrupo.Foto;
            return PartialView(aulaGrupo);
        }

        // POST: AulaGrupoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditAula(int id, IFormCollection colletion, [Bind("IdAulaGrupo,IdAdmin,Nome,Descricao")] AulaGrupo aulaGrupo)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    AulaGrupo aulaOld = _context.AulaGrupo.FirstOrDefault(x => x.IdAulaGrupo == aulaGrupo.IdAulaGrupo);
                    if (colletion.Files.Count != 0)
                    {
                        IFormFile file = colletion.Files.First();
                        string caminho = Path.Combine(_hostEnvironment.ContentRootPath, "wwwroot\\img\\FotosAulas");
                        string nome_ficheiro = Path.GetFileName(file.FileName);
                        string caminho_completo = Path.Combine(caminho, nome_ficheiro);
                        FileStream fs = new FileStream(caminho_completo, FileMode.Create);
                        file.CopyTo(fs);
                        fs.Close();
                        aulaOld.Foto = nome_ficheiro;
                    }
                    int idAdmin = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                    aulaOld.IdAdminNavigation = _context.Administrador.FirstOrDefault(x => x.IdAdmin == idAdmin);
                    aulaOld.IdAdmin = idAdmin;
                    aulaOld.Descricao = aulaGrupo.Descricao;
                    aulaOld.Nome = aulaGrupo.Nome;
                    _context.AulaGrupo.Update(aulaOld);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AulaGrupoExists(aulaGrupo.IdAulaGrupo))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(ListAulas));
            }
            ViewData["IdAdmin"] = new SelectList(_context.Administrador, "IdAdmin", "PalavraPass", aulaGrupo.IdAdmin);
            return View(aulaGrupo);
        }

        private bool AulaGrupoExists(int id)
        {
            return _context.AulaGrupo.Any(e => e.IdAulaGrupo == id);
        }

        public async Task<IActionResult> IndexMapaAulas()
        {
            var aulas = _context.Aula.Include(x => x.IdAulaGrupoNavigation).Include(x => x.IdAdminNavigation).Include(x => x.IdProfessorNavigation.IdUtilizadorNavigation);
            ViewData["IdProfessor"] = _context.Professor.Select(a =>
                            new SelectListItem
                            {
                                Value = a.IdProfessor.ToString(),
                                Text = a.IdUtilizadorNavigation.Nome,
                            }).ToList();
            return View(await aulas.ToListAsync());
        }

        public IActionResult CreateInstAula()
        {
            ViewData["IdAulaGrupo"] = new SelectList(_context.AulaGrupo, "IdAulaGrupo", "Nome");

            var profs = _context.Professor.Include(x => x.IdUtilizadorNavigation).ToList();

            ViewData["IdProfessor"] = _context.Professor.Select(a =>
                               new SelectListItem
                               {
                                   Value = a.IdProfessor.ToString(),
                                   Text = a.IdUtilizadorNavigation.Nome,
                               }).ToList();

            return View();
        }

        // POST: Aulas1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateInstAula([Bind("IdAula,IdProfessor,IdAulaGrupo,IdAdmin,Lotacao,ValidadeAte,ValidadeDe,DiaDaSemana,Semana,Hora")] Aula aula)
        {
            if (ModelState.IsValid)
            {
                int idAdmin = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                aula.IdAdmin = idAdmin;
                aula.IdAdminNavigation = _context.Administrador.FirstOrDefault(x => x.IdAdmin == idAdmin);
                aula.IdProfessorNavigation = _context.Professor.FirstOrDefault(x => x.IdProfessor == aula.IdProfessor);
                _context.Aula.Add(aula);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(IndexMapaAulas));
            }
            ViewData["IdAdmin"] = new SelectList(_context.Administrador, "IdAdmin", "PalavraPass", aula.IdAdmin);
            ViewData["IdAulaGrupo"] = new SelectList(_context.AulaGrupo, "IdAulaGrupo", "Descricao", aula.IdAulaGrupo);
            ViewData["IdProfessor"] = new SelectList(_context.Professor, "IdProfessor", "Especialidade", aula.IdProfessor);
            return View(aula);
        }
        public async Task<IActionResult> EditInstAula(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aula = await _context.Aula.FindAsync(id);
            if (aula == null)
            {
                return NotFound();
            }
            ViewData["IdAulaGrupo"] = new SelectList(_context.AulaGrupo, "IdAulaGrupo", "Nome", aula.IdAulaGrupo);
            ViewData["IdProfessor"] = _context.Professor.Select(a =>
                               new SelectListItem
                               {
                                   Value = a.IdProfessor.ToString(),
                                   Text = a.IdUtilizadorNavigation.Nome,
                               }).ToList();
            return View(aula);
        }

        // POST: Aulas1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditInstAula(int id, [Bind("IdAula,IdProfessor,IdAulaGrupo,IdAdmin,Lotacao,ValidadeAte,ValidadeDe,DiaDaSemana,Semana,Hora")] Aula aula)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    int idAdmin = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                    aula.IdAdmin = idAdmin;
                    aula.IdAdminNavigation = _context.Administrador.FirstOrDefault(x => x.IdAdmin == idAdmin);
                    aula.IdProfessorNavigation = _context.Professor.FirstOrDefault(x => x.IdProfessor == aula.IdProfessor);
                    _context.Update(aula);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AulaExists(aula.IdAula))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(IndexMapaAulas));
            }
            ViewData["IdAdmin"] = new SelectList(_context.Administrador, "IdAdmin", "PalavraPass", aula.IdAdmin);
            ViewData["IdAulaGrupo"] = new SelectList(_context.AulaGrupo, "IdAulaGrupo", "Descricao", aula.IdAulaGrupo);
            ViewData["IdProfessor"] = new SelectList(_context.Professor, "IdProfessor", "Especialidade", aula.IdProfessor);
            return View(aula);
        }
        private bool AulaExists(int id)
        {
            return _context.Aula.Any(e => e.IdAula == id);
        }
        // #################### Zona Socios #########################
        public async Task<IActionResult> ListSocios()
        {
            var weWonderGymDataBaseContext = _context.Socio.Include(s => s.IdUtilizadorNavigation);
            return View(await weWonderGymDataBaseContext.ToListAsync());
        }

        [HttpGet]
        public IActionResult AlterarUtlEstado(int id)
        {
            var utl = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == id);

            return PartialView(utl);
        }

        [HttpPost]
        public IActionResult AlterarUtlEstado([Bind("IdUtilizador,Estado,EstadoMotivo")] Utilizador utilizador)
        {
           var utl = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == utilizador.IdUtilizador);
            //if (utl.Estado == 0)
            //    utl.Estado = 3;
            //else
            //    utl.Estado = 0;

            int idAdmin = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            utl.IdAdmin = idAdmin;
            utl.IdAdminNavigation = _context.Administrador.FirstOrDefault(x => x.IdAdmin == idAdmin);
            utl.EstadoMotivo = utilizador.EstadoMotivo;
            utl.Estado = utilizador.Estado;
            _context.Utilizador.Update(utl);

            _context.SaveChanges();
            var weWonderGymDataBaseContext = _context.Socio.Include(s => s.IdUtilizadorNavigation);
            return PartialView("ListSociosPartial", weWonderGymDataBaseContext);
        }
        public IActionResult AtivarUtl(int id)
        {
            var utl = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == id);
            if (utl.Estado == 2)
                utl.Estado = 3;

            string newPass = RandomString(7);


            SHA512 sha512 = SHA512Managed.Create();
            byte[] bytes = sha512.ComputeHash(Encoding.UTF8.GetBytes(newPass), 0, Encoding.UTF8.GetByteCount(newPass));
            string passHash = Convert.ToBase64String(bytes);

            utl.PalavraPass = passHash;
            _context.Utilizador.Update(utl);
            _context.SaveChanges();


            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                mail.From = new MailAddress("wewondergym123@gmail.com");
                mail.To.Add(utl.Email);
                mail.Subject = "Confirmação da ativação de Sócio";
                mail.Body = "Sr/a " + utl.Nome + " a sua conta de sócio ao ginásio WeWonderGym já se encontra ativa!" + System.Environment.NewLine + "Password: " + newPass + System.Environment.NewLine + "Cumprimentos WeWonderGym!";

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("wewondergym123@gmail.com", "WeWonder_123");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);

            }
            catch (Exception ex)
            {

            }



            var weWonderGymDataBaseContext = _context.Socio.Include(s => s.IdUtilizadorNavigation);
            return PartialView("ListSociosPartial", weWonderGymDataBaseContext);
        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        //######################## Zona Exercicios #######################
        public async Task<IActionResult> ListExercicios()
        {
            var weWonderGymDataBaseContext = _context.ExercicioGinasio.Include(e => e.IdAdminNavigation);
            return View(await weWonderGymDataBaseContext.ToListAsync());
        }
        public IActionResult CreateExercicio()
        {
            ViewData["IdAdmin"] = new SelectList(_context.Administrador, "IdAdmin", "PalavraPass");
            return View();
        }

        // POST: ExercicioGinasios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateExercicio(IFormCollection colletion, [Bind("IdExercicio,Foto,Nome,Descricao,Video")] ExercicioGinasio exercicioGinasio)
        {
            if (ModelState.IsValid)
            {
                IFormFile file = colletion.Files.GetFile(name: "Foto");
                string caminho = Path.Combine(_hostEnvironment.ContentRootPath, "wwwroot\\img\\imgExercicios\\Fotos");
                string nome_ficheiro = Path.GetFileName(file.FileName);
                nome_ficheiro = exercicioGinasio.Nome + exercicioGinasio.Descricao + nome_ficheiro;
                string caminho_completo = Path.Combine(caminho, nome_ficheiro);
                FileStream fs = new FileStream(caminho_completo, FileMode.Create);
                file.CopyTo(fs);
                fs.Close();

                IFormFile fileVideo = colletion.Files.GetFile(name: "Video");
                string caminhoVideo = Path.Combine(_hostEnvironment.ContentRootPath, "wwwroot\\img\\imgExercicios\\Video");
                string nome_video = Path.GetFileName(fileVideo.FileName);
                nome_video = exercicioGinasio.Nome + exercicioGinasio.Descricao + nome_video;
                string caminho_completoVideo = Path.Combine(caminhoVideo, nome_video);
                using (var fsVideo = new FileStream(caminho_completoVideo, FileMode.Create))
                {
                    await file.CopyToAsync(fsVideo);
                }
                   
                fs.Close();


                exercicioGinasio.Foto = nome_ficheiro;
                exercicioGinasio.Video = nome_video;
                int idAdmin = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                exercicioGinasio.IdAdmin = idAdmin;
                exercicioGinasio.IdAdminNavigation = _context.Administrador.FirstOrDefault(x => x.IdAdmin == idAdmin);
                _context.ExercicioGinasio.Add(exercicioGinasio);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(ListExercicios));
            }
            ViewData["IdAdmin"] = new SelectList(_context.Administrador, "IdAdmin", "PalavraPass", exercicioGinasio.IdAdmin);
            return View(exercicioGinasio);
        }
        public IActionResult DeleteExercio(int Id)
        {
            ExercicioGinasio p = _context.ExercicioGinasio.FirstOrDefault(x => x.IdExercicio == Id);

            string caminho = Path.Combine(_hostEnvironment.ContentRootPath, "wwwroot\\img\\imgExercicios\\Fotos");
            string nome_ficheiro = p.Foto;
            string caminho_completo = Path.Combine(caminho, nome_ficheiro);
            System.IO.File.Delete(caminho_completo);

            string caminhoVideo = Path.Combine(_hostEnvironment.ContentRootPath, "wwwroot\\img\\imgExercicios\\Fotos");
            string nome_ficheiroVideo = p.Video;
            string caminho_completoVideo = Path.Combine(caminhoVideo, nome_ficheiroVideo);
            System.IO.File.Delete(caminho_completoVideo);

            _context.ExercicioGinasio.Remove(p);

            _context.SaveChanges();
            var weWonderGymDataBaseContext = _context.ExercicioGinasio.Include(e => e.IdAdminNavigation);
            return PartialView("ListExerciciosPartial", weWonderGymDataBaseContext);

        }

        public async Task<IActionResult> DeleteAula(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aula = await _context.Aula
                .Include(a => a.IdAdminNavigation)
                .Include(a => a.IdAulaGrupoNavigation)
                .Include(a => a.IdProfessorNavigation)
                .FirstOrDefaultAsync(m => m.IdAula == id);
            if (aula == null)
            {
                return NotFound();
            }
            var aulaDel = await _context.Aula.FindAsync(id);

            var DelIncrever = _context.Inscrever.Where(x => x.IdAula == id);

            foreach (var a in DelIncrever)
                _context.Inscrever.Remove(a);

            _context.Aula.Remove(aulaDel);

            await _context.SaveChangesAsync();
            return RedirectToAction("IndexMapaAulas");
        }

        public IActionResult DetailsProfessor(int? id)
        {
            var professor = _context.Professor.FirstOrDefault(e => e.IdProfessor == id);

            var utilzador = _context.Utilizador.FirstOrDefault(e=>e.IdUtilizador == professor.IdUtilizador);

             return PartialView(utilzador);
        }


    }
}
