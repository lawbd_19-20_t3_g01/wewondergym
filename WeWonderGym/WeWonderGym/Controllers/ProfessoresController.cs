﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Hosting;
using WeWonderGym.Data;
using WeWonderGym.Models;
using Microsoft.EntityFrameworkCore;
using WeWonderGym.Filters;

namespace WeWonderGym.Controllers
{
    [RoleFilter(Perfil = "Professor")]
    public class ProfessoresController : Controller
    {
        private readonly WeWonderGymDataBaseContext _context;
        private readonly IHostEnvironment _hostEnvironment;

        public ProfessoresController(WeWonderGymDataBaseContext context, IHostEnvironment hostEnvironment)
        {
            _context = context;
            _hostEnvironment = hostEnvironment;
        }
        public ActionResult Index()
        {
            return View();
        }

        //#############Perfil###################
        public ActionResult IndexPerfil()
        {
            var utilizador = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == Convert.ToInt32(HttpContext.Session.GetInt32("UserId")));
            return View(utilizador);
        }


        [HttpPost]
        public async Task<IActionResult> IndexPerfil(IFormCollection colletion, [Bind("NumTelefone,Fotografica,PalavraPass")] Utilizador utilizador, string OldPass)
        {
            Utilizador edit = _context.Utilizador.Find(Convert.ToInt32(HttpContext.Session.GetInt32("UserId")));
            SHA512 sha512 = SHA512Managed.Create();
            if (utilizador.PalavraPass != null)
            {
                byte[] bytes = sha512.ComputeHash(Encoding.UTF8.GetBytes(utilizador.PalavraPass), 0, Encoding.UTF8.GetByteCount(utilizador.PalavraPass));
                string passHash = Convert.ToBase64String(bytes);
                utilizador.PalavraPass = passHash;
            }
            if (OldPass != null)
            {
                byte[] bytesOldPass = sha512.ComputeHash(Encoding.UTF8.GetBytes(OldPass), 0, Encoding.UTF8.GetByteCount(OldPass));
                OldPass = Convert.ToBase64String(bytesOldPass);
            }


            if ((utilizador.PalavraPass != null && OldPass != null))
            {
                if (edit.PalavraPass != OldPass)
                    ModelState.AddModelError("OldPass", "Password incorreta!");
            }

            if ((utilizador.PalavraPass == null && OldPass != null) || (utilizador.PalavraPass != null && OldPass == null))
            {
                ModelState.AddModelError("OldPass", "Tem que preencher ambos os campos se deseja mudar a Password");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var file = colletion.Files.First();
                    if (file.ContentType != null)
                    {
                        string caminho = Path.Combine(_hostEnvironment.ContentRootPath, "wwwroot\\img\\Fotos");
                        string nome_ficheiro = Path.GetFileName(file.FileName);
                        string caminho_completo = Path.Combine(caminho, nome_ficheiro);
                        FileStream fs = new FileStream(caminho_completo, FileMode.Create);
                        file.CopyTo(fs);
                        fs.Close();


                        string caminho_deleteC = Path.Combine(caminho, edit.Fotografica);


                        System.IO.File.Delete(caminho_deleteC);

                        edit.Fotografica = nome_ficheiro;
                    }

                    edit.NumTelefone = utilizador.NumTelefone;

                    if (utilizador.PalavraPass != null)
                        edit.PalavraPass = utilizador.PalavraPass;

                    _context.Update(edit);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("IndexPerfil");
                }
                catch (Exception)
                {
                    edit.NumTelefone = utilizador.NumTelefone;

                    if (utilizador.PalavraPass != null)
                        edit.PalavraPass = utilizador.PalavraPass;

                    _context.Update(edit);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("IndexPerfil");
                }
            }
            var utl = _context.Utilizador.FirstOrDefault(x => x.IdUtilizador == Convert.ToInt32(HttpContext.Session.GetInt32("UserId")));
            return View(utl);
        }
        //########################################

        //#############Planos###################
        public async Task<IActionResult> IndexPlanos()
        {
            var id = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            var prof = _context.Professor.FirstOrDefault(x => x.IdUtilizador == id);
            var weWonderGymDataBaseContext = _context.PlanoExercicio.Where(x => x.IdProfessor == prof.IdProfessor).Include(p => p.IdSocioNavigation.IdUtilizadorNavigation).Where(x => x.IdSocioNavigation.IdUtilizadorNavigation.Estado == 1);
            return View(await weWonderGymDataBaseContext.ToListAsync());
        }

        public IActionResult CreatePlano()
        {
            ViewData["IdSocio"] = _context.Socio.Include(x => x.IdUtilizadorNavigation).Where(x => x.IdUtilizadorNavigation.Estado == 1).Select(a =>
                           new SelectListItem
                           {
                               Value = a.IdSocio.ToString(),
                               Text = a.IdUtilizadorNavigation.Email,
                           }).ToList();

            return View();
        }

        // POST: PlanoExercicios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreatePlano([Bind("IdPlano,IdProfessor,IdSocio,Descricao,Ativo")] PlanoExercicio planoExercicio)
        {
            if (ModelState.IsValid)
            {

                int utlId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                var prof = _context.Professor.FirstOrDefault(x => x.IdUtilizador == utlId);


                planoExercicio.IdProfessorNavigation = prof;
                planoExercicio.IdProfessor = prof.IdProfessor;
                planoExercicio.Ativo = true;
                _context.Add(planoExercicio);
                await _context.SaveChangesAsync();
                return RedirectToAction("IndexPlanos");
            }
            ViewData["IdProfessor"] = new SelectList(_context.Professor, "IdProfessor", "Especialidade", planoExercicio.IdProfessor);
            ViewData["IdSocio"] = new SelectList(_context.Socio, "IdSocio", "Altura", planoExercicio.IdSocio);
            return View(planoExercicio);
        }

        public IActionResult CreateInstExercicio(int idPlano)
        {
            ViewData["IdExercicio"] = new SelectList(_context.ExercicioGinasio, "IdExercicio", "Nome");
            return View();
        }

        // POST: ExercicoPlanoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateInstExercicio(int idPlano, [Bind("IdExercicio,IdPlano,DuracaoPeriodoDescanso,QuantidadeSeries,NumeroRepeticoes,DuracaoExercicios,Ordem")] ExercicoPlano exercicoPlano)
        {
            var ordem = _context.ExercicoPlano.FirstOrDefault(x => x.Ordem == exercicoPlano.Ordem && x.IdPlano == exercicoPlano.IdPlano);



            if (ordem != null)
            {
                ModelState.AddModelError("Erro", "Não pode ter dois exercicios com a mesma ordem!");
                ViewBag.Sucess = "erro";

            }


            if (ModelState.IsValid)
            {
                exercicoPlano.IdPlanoNavigation = _context.PlanoExercicio.FirstOrDefault(x => x.IdPlano == idPlano);
                exercicoPlano.IdPlano = idPlano;
                _context.Add(exercicoPlano);
                await _context.SaveChangesAsync();
                ViewBag.Sucess = "sucesso";
                ViewData["IdExercicio"] = new SelectList(_context.ExercicioGinasio, "IdExercicio", "Descricao", exercicoPlano.IdExercicio);
                return PartialView(exercicoPlano);
            }
            ViewData["IdExercicio"] = new SelectList(_context.ExercicioGinasio, "IdExercicio", "Descricao", exercicoPlano.IdExercicio);
            ViewData["IdPlano"] = new SelectList(_context.PlanoExercicio, "IdPlano", "Descricao", exercicoPlano.IdPlano);
            return PartialView(exercicoPlano);
        }

        public async Task<IActionResult> ListExercicios(int id)
        {
            var weWonderGymDataBaseContext = _context.ExercicoPlano.Include(e => e.IdExercicioNavigation).Include(e => e.IdPlanoNavigation).Where(x => x.IdPlano == id);
            return View(await weWonderGymDataBaseContext.OrderBy(x => x.Ordem).ToListAsync());
        }

        public IActionResult EliminarExercicios(int? id)
        {
            var remove = _context.ExercicoPlano.FirstOrDefault(x => x.IdExercicio == id);
            _context.ExercicoPlano.Remove(remove);
            _context.SaveChanges();

            return PartialView("ListPartialExercicios", _context.ExercicoPlano.Include(e => e.IdExercicioNavigation).Include(e => e.IdPlanoNavigation).OrderBy(x => x.Ordem));
        }


        public async Task<IActionResult> EditExercicos(int? id)
        {
            ExercicoPlano edit = _context.ExercicoPlano.FirstOrDefault(x => x.IdExercicio == id);

            if (id == null)
            {
                return NotFound();
            }

            var exercicoPlano = await _context.ExercicoPlano.FindAsync(id, edit.IdPlano, edit.Ordem);
            if (exercicoPlano == null)
            {
                return NotFound();
            }
            ViewData["IdExercicio"] = new SelectList(_context.ExercicioGinasio, "IdExercicio", "Descricao", exercicoPlano.IdExercicio);
            ViewData["IdPlano"] = new SelectList(_context.PlanoExercicio, "IdPlano", "Descricao", exercicoPlano.IdPlano);
            return PartialView("EditExercicos", exercicoPlano);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditExercicos(int id, [Bind("IdExercicio,IdPlano,DuracaoPeriodoDescanso,QuantidadeSeries,NumeroRepeticoes,DuracaoExercicios,Ordem")] ExercicoPlano exercicoPlano)
        {
            if (id != exercicoPlano.IdExercicio)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(exercicoPlano);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ExercicoPlanoExists(exercicoPlano.IdExercicio))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                var weWonderGymDataBaseContext = _context.ExercicoPlano.Include(e => e.IdExercicioNavigation).Include(e => e.IdPlanoNavigation);
                return PartialView("ListPartialExercicios", weWonderGymDataBaseContext.OrderBy(x => x.Ordem));
            }
            ViewData["IdExercicio"] = new SelectList(_context.ExercicioGinasio, "IdExercicio", "Descricao", exercicoPlano.IdExercicio);
            ViewData["IdPlano"] = new SelectList(_context.PlanoExercicio, "IdPlano", "Descricao", exercicoPlano.IdPlano);
            return PartialView(exercicoPlano);
        }

        private bool ExercicoPlanoExists(int id)
        {
            return _context.ExercicoPlano.Any(e => e.IdExercicio == id);
        }

        public async Task<IActionResult> EditEstadoPlano(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var planoExercicio = await _context.PlanoExercicio.FindAsync(id);


            if (planoExercicio == null)
            {
                return NotFound();
            }
            if (planoExercicio.Ativo == true)
            {
                planoExercicio.Ativo = false;
            }
            else
            {
                planoExercicio.Ativo = true;
            }
            await _context.SaveChangesAsync();
            ViewData["IdProfessor"] = new SelectList(_context.Professor, "IdProfessor", "Especialidade", planoExercicio.IdProfessor);
            ViewData["IdSocio"] = new SelectList(_context.Socio, "IdSocio", "Altura", planoExercicio.IdSocio);
            return RedirectToAction("IndexPlanos");
        }

        //###########AULAS########
        public async Task<IActionResult> IndexAulas()
        {
            var id = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            var prof = _context.Professor.FirstOrDefault(x=>x.IdUtilizador == id);
            var weWonderGymDataBaseContext = _context.Aula.Where(x => x.IdProfessor == prof.IdProfessor).Include(a => a.IdAdminNavigation).Include(a => a.IdAulaGrupoNavigation).Include(a => a.IdProfessorNavigation);
            return View(await weWonderGymDataBaseContext.ToListAsync());
        }

        public async Task<IActionResult> ListSociosAula(int? id)
        {
            var weWonderGymDataBaseContext = _context.Inscrever.Where(x => x.IdAula == id).Include(p => p.IdSocioNavigation).Include(x => x.IdSocioNavigation.IdUtilizadorNavigation);

            return View(await weWonderGymDataBaseContext.OrderBy(x => x.IdSocioNavigation.IdUtilizadorNavigation.Nome).ToListAsync());
        }


        //###########Socios/PT########

        public IActionResult IndexSociosPT()
        {
            return View();
        }

        public async Task<IActionResult> ListSociosPT()
        {
            var id = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            var prof = _context.Professor.FirstOrDefault(x => x.IdUtilizador == id);
            var weWonderGymDataBaseContext = _context.PedidoPt.Where(x => x.IdProfessor == prof.IdProfessor).Include(p => p.IdProfessorNavigation).Include(p => p.IdSocioNavigation.Peso).Include(p => p.IdSocioNavigation.IdUtilizadorNavigation);

            return View(await weWonderGymDataBaseContext.ToListAsync());
        }


        public async Task<IActionResult> ListHistorico(int? id)
        {
            var weWonderGymDataBaseContext = _context.Inscrever.Where(x => x.IdSocio == id).Include(p => p.IdAulaNavigation).Include(p => p.IdAulaNavigation.IdAulaGrupoNavigation);
            return View(await weWonderGymDataBaseContext.OrderBy(x => x.DataAula).ToListAsync());
        }



        //##################Peso#############
        public async Task<IActionResult> Pesagem()
        {

            if (_context.Peso.Any(p => p.IdSocioNavigation != null))
            {
                var weWonderGymDataBaseContext = _context.Peso.Include(p => p.IdSocioNavigation.IdUtilizadorNavigation);
                return View(await weWonderGymDataBaseContext.OrderBy(p => p.IdSocioNavigation.IdUtilizadorNavigation.Nome).ToListAsync());
            }
            else
            {
                return View(null);
            }


        }

        public async Task<IActionResult> ListSociosPesagem(string email)
        {
            var weWonderGymDataBaseContext = _context.Socio.Include(s => s.IdUtilizadorNavigation).Where(s => s.IdUtilizadorNavigation.Email.StartsWith(email));

            if (weWonderGymDataBaseContext.Count() == 0)
            {
                ViewBag.Sucess = "erro";
                return PartialView(await weWonderGymDataBaseContext.OrderBy(p => p.IdUtilizadorNavigation.Nome).ToListAsync());
            }

            return PartialView(await weWonderGymDataBaseContext.OrderBy(p => p.IdUtilizadorNavigation.Nome).ToListAsync());
        }


        public IActionResult CreatePeso(int id)
        {
            ViewData["IdProfessor"] = new SelectList(_context.Professor, "IdProfessor", "Especialidade");
            ViewData["IdSocio"] = new SelectList(_context.Socio, "IdSocio", "Altura");
            return PartialView();
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreatePeso(int id, [Bind("IdProfessor,IdSocio,DataCriacao,Valor")] Peso peso)
        {
            if (ModelState.IsValid)
            {
                peso.IdSocio = id;
                var idUtilizador = HttpContext.Session.GetInt32("UserId").GetValueOrDefault();
                var Prof = _context.Professor.FirstOrDefault(x => x.IdUtilizador == idUtilizador);
                peso.IdProfessor = Prof.IdProfessor;
                peso.DataCriacao = DateTime.Now;


                _context.Add(peso);
                await _context.SaveChangesAsync();
                return RedirectToAction("Pesagem");
            }
            ViewData["IdProfessor"] = new SelectList(_context.Professor, "IdProfessor", "Especialidade", peso.IdProfessor);
            ViewData["IdSocio"] = new SelectList(_context.Socio, "IdSocio", "Altura", peso.IdSocio);
            return PartialView(peso);
        }



    }




}