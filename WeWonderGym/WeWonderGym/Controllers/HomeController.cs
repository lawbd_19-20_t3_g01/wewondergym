﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WeWonderGym.Data;
using WeWonderGym.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace WeWonderGym.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly WeWonderGymDataBaseContext _context;

        public HomeController(ILogger<HomeController> logger, WeWonderGymDataBaseContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            GinasioAulas supermodel = new GinasioAulas();
            supermodel.AulaModel = _context.Aula.Include(x => x.IdAulaGrupoNavigation).Include(x => x.IdAdminNavigation).Include(x => x.IdProfessorNavigation.IdUtilizadorNavigation).ToList();
            supermodel.GinasioModel = _context.Ginasio.First(x => x.IdGinasio == 0);
            return View(supermodel);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult MapaDeAulas()
        {
            return PartialView(_context.Aula.Include(x => x.IdAulaGrupoNavigation).ToList());
        }

    }
}
