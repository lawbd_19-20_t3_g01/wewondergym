﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeWonderGym.Controllers;
using WeWonderGym.Data;

namespace WeWonderGym.Filters
{
    public class RoleFilter : ActionFilterAttribute
    {
        public string Perfil { get; set; }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (UtilizadoresController.estaAutenticado(context.HttpContext))
            {
                if (context.HttpContext.Session.GetString("Perfil") == Perfil)
                    base.OnActionExecuting(context);
                else
                {
                    Controller c = (context.Controller as Controller);
                    c.ViewData["mensagem"] = "Necessita de ter Perfil " + Perfil;
                    context.Result = new ViewResult { StatusCode = 401, ViewName = "Erro", ViewData = c.ViewData };
                }
            }
            else
            {
                Controller c = (context.Controller as Controller);
                c.ViewData["mensagem"] = "Necessita estar autenticado";
                context.Result = new ViewResult { StatusCode = 401, ViewName = "Erro", ViewData = c.ViewData };
            }
        }
    }
}

